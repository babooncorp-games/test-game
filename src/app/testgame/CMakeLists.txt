add_executable(testgame main.cpp testgameapp.cpp testgameappdependentmodule.cpp)

target_link_libraries(testgame
PUBLIC
	subgine::actor
	subgine::actor-physic
	subgine::animation
	subgine::aseprite
	subgine::audio
	subgine::bag
	subgine::behavior
	subgine::collision
	subgine::collision-physic
	subgine::common
	subgine::entity
	subgine::event
	subgine::gameloop
	subgine::graphic
	subgine::log
	subgine::opengl
	subgine::physic
	subgine::provider
	subgine::resource
	subgine::scene
	subgine::shape
	subgine::state
	subgine::system
	subgine::text
	subgine::tiled-aseprite
	subgine::tiled
	subgine::tiled-opengl
	subgine::vector
	subgine::window

	testgame::bundle
	testgame::menu
	testgame::simple-game
	testgame::space-shooter
	testgame::test-bag
	testgame::test-aseprite
	testgame::test-tiled
	testgame::test-behavior
	testgame::test-collision
	testgame::asset
	testgame::bomb
	testgame::player
	testgame::render
	
PRIVATE
	subgine::common-warnings
)

set_target_properties(testgame PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

subgine_config_asset_directory(testgame)
subgine_copy_required_dlls(testgame)
