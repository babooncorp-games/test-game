#pragma once

#include "../testgameappdependentmodule.h"

#include "subgine/common/kangaru.h"

namespace testgame {

struct TestGameAppDependentModuleService : kgr::single_service<TestGameApp::DependentModule, kgr::dependency<kgr::container_service>> {};

auto service_map(TestGameApp::DependentModule) -> TestGameAppDependentModuleService;

} // namespace testgame
