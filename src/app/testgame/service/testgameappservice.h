#pragma once

#include "../testgameapp.h"

#include "subgine/common/kangaru.h"
#include "testgameappdependentmoduleservice.h"

#include "subgine/window/service/windowservice.h"
#include "subgine/scene/service/scenemanagerservice.h"
#include "subgine/gameloop/service/gameloopservice.h"

namespace testgame {

struct TestGameAppService : kgr::single_service<TestGameApp, kgr::autowire> {};

auto service_map(const TestGameApp&) -> TestGameAppService;

} // namespace testgame
