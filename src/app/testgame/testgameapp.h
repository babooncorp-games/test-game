#pragma once

#include "subgine/common/kangaru.h"

namespace sbg {
	struct Window;
	struct Gameloop;
	struct SceneManager;
	struct GameDatabase;
	struct WindowService;
	struct GameloopService;
	struct SceneManagerService;
	struct GameDatabaseService;
}

namespace testgame {

struct TestGameApp {
	struct DependentModule;
	
	TestGameApp(
		DependentModule&,
		kgr::lazy<sbg::WindowService> window,
		kgr::lazy<sbg::GameloopService> gameloop,
		kgr::lazy<sbg::SceneManagerService> scenes,
		kgr::lazy<sbg::GameDatabaseService> database
	);
	
	int run(int argc, char** argv);
	
private:
	sbg::Window& _window;
	sbg::Gameloop& _gameloop;
	sbg::SceneManager& _scenes;
	sbg::GameDatabase& _database;
};

} // namespace testgame
