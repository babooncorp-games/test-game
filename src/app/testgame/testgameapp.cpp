#include "testgameapp.h"

#include "subgine/window.h"
#include "subgine/resource.h"
#include "subgine/scene.h"
#include "subgine/gameloop.h"

#include "subgine/window/service.h"
#include "subgine/resource/service.h"
#include "subgine/scene/service.h"
#include "subgine/gameloop/service.h"

#include <string_view>

using namespace std::literals;


namespace testgame {

TestGameApp::TestGameApp(
	DependentModule&,
	kgr::lazy<sbg::WindowService> window,
	kgr::lazy<sbg::GameloopService> gameloop,
	kgr::lazy<sbg::SceneManagerService> scenes,
	kgr::lazy<sbg::GameDatabaseService> database
) :
	_window{*window},
	_gameloop{*gameloop},
	_scenes{*scenes},
	_database{*database} {}

int TestGameApp::run(int argc, char** argv) {
	_window.open(sbg::WindowSettings{u8"🧪 testgame", {1280, 720}, false});
	
	_database.load("scene.cbor");
	
	auto const scene_name = argc == 2 ? std::string_view{argv[1]} : "menu"sv;
	auto& scene = _scenes.create(scene_name);
	
	_scenes.plug(scene);
	_scenes.load(scene);
	
	_gameloop.run();
	
	return 0;
}

} // namespace testgame
