#include "service/testgameappservice.h"

int main(int argc, char** argv) {
	return kgr::container{}.service<testgame::TestGameAppService>().run(argc, argv);
}
