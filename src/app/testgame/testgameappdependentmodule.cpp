#include "testgameappdependentmodule.h"

#include "testgame/bundle/module.h"
#include "testgame/menu/module.h"
#include "course/asset/module.h"
#include "course/player/module.h"
#include "course/bomb/module.h"
#include "course/render/module.h"
#include "testgame/simple-game/module.h"
#include "testgame/space-shooter/module.h"
#include "testgame/test-bag/module.h"
#include "testgame/test-aseprite/module.h"
#include "testgame/test-tiled/module.h"
#include "testgame/test-behavior/module.h"
#include "testgame/test-collision/module.h"

#include "subgine/actor/module.h"
#include "subgine/actor-physic/module.h"
#include "subgine/animation/module.h"
#include "subgine/aseprite/module.h"
#include "subgine/audio/module.h"
#include "subgine/bag/module.h"
#include "subgine/behavior/module.h"
#include "subgine/collision/module.h"
#include "subgine/collision-physic/module.h"
#include "subgine/common/module.h"
#include "subgine/entity/module.h"
#include "subgine/event/module.h"
#include "subgine/gameloop/module.h"
#include "subgine/graphic/module.h"
#include "subgine/log/module.h"
#include "subgine/opengl/module.h"
#include "subgine/physic/module.h"
#include "subgine/provider/module.h"
#include "subgine/resource/module.h"
#include "subgine/scene/module.h"
#include "subgine/shape/module.h"
#include "subgine/state/module.h"
#include "subgine/system/module.h"
#include "subgine/text/module.h"
#include "subgine/tiled-aseprite/module.h"
#include "subgine/tiled/module.h"
#include "subgine/tiled-opengl/module.h"
#include "subgine/vector/module.h"
#include "subgine/window/module.h"

namespace testgame {

TestGameApp::DependentModule::DependentModule(kgr::container& container) {
	container.service<sbg::ActorModuleService>();
	container.service<sbg::ActorPhysicModuleService>();
	container.service<sbg::AnimationModuleService>();
	container.service<sbg::AsepriteModuleService>();
	container.service<sbg::AudioModuleService>();
	container.service<sbg::BagModuleService>();
	container.service<sbg::BehaviorModuleService>();
	container.service<sbg::CollisionModuleService>();
	container.service<sbg::CollisionPhysicModuleService>();
	container.service<sbg::CommonModuleService>();
	container.service<sbg::EntityModuleService>();
	container.service<sbg::EventModuleService>();
	container.service<sbg::GameloopModuleService>();
	container.service<sbg::GraphicModuleService>();
	container.service<sbg::LogModuleService>();
	container.service<sbg::OpenglModuleService>();
	container.service<sbg::PhysicModuleService>();
	container.service<sbg::ProviderModuleService>();
	container.service<sbg::ResourceModuleService>();
	container.service<sbg::SceneModuleService>();
	container.service<sbg::ShapeModuleService>();
	container.service<sbg::StateModuleService>();
	container.service<sbg::SystemModuleService>();
	container.service<sbg::TextModuleService>();
	container.service<sbg::TiledAsepriteModuleService>();
	container.service<sbg::TiledModuleService>();
	container.service<sbg::TiledOpenglModuleService>();
	container.service<sbg::VectorModuleService>();
	container.service<sbg::WindowModuleService>();
	
	container.service<BundleModuleService>();
	container.service<MenuModuleService>();
	container.service<simplegame::SimpleGameModuleService>();
	container.service<spaceshooter::SpaceShooterModuleService>();
	container.service<course::AssetModuleService>();
	container.service<course::PlayerModuleService>();
	container.service<course::BombModuleService>();
	container.service<course::RenderModuleService>();
	container.service<testbag::TestBagModuleService>();
	container.service<testaseprite::TestAsepriteModuleService>();
	container.service<testtiled::TestTiledModuleService>();
	container.service<testbehavior::TestBehaviorModuleService>();
	container.service<testcollision::TestCollisionModuleService>();
}

} // namespace testgame
