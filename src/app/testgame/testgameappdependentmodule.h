#pragma once

#include "testgameapp.h"

#include "subgine/common/kangaru.h"

namespace testgame {

struct TestGameApp::DependentModule {
	DependentModule(kgr::container& container);
};

} // namespace testgame
