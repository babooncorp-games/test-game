#pragma once

namespace sbg {
	struct ViewCreator;
}

namespace course {

struct RenderModule {
	void setupViewCreator(sbg::ViewCreator& viewCreator);
};

} // namespace course
