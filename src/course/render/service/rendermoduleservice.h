#pragma once

#include "../rendermodule.h"

#include "subgine/graphic/service/viewcreatorservice.h"

namespace course {
	struct RenderModuleService : kgr::single_service<RenderModule, kgr::autowire>,
		sbg::autocall<
			&RenderModule::setupViewCreator
		> {};
}
