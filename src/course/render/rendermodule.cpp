#include "rendermodule.h"

#include "subgine/graphic.h"
#include "subgine/opengl.h"
#include "subgine/graphic/service.h"
#include "subgine/opengl/service.h"
#include "subgine/system/service.h"

#include "glbinding/gl33/gl.h"

using namespace course;

void RenderModule::setupViewCreator(sbg::ViewCreator& viewCreator) {
	viewCreator.add("post-processing", [](sbg::Camera const& camera, sbg::MainEngine& mainEngine, sbg::ProgramFactory programs, sbg::SharedArrayMeshFactory meshes) {
		auto render = sbg::OwnedResource{sbg::Texture{gl::GL_TEXTURE_2D}};
		
		render->create();
		render->image(sbg::TextureFormat2D{0, gl::GL_SRGB_ALPHA, {1366, 768}, gl::GL_RGBA, gl::GL_UNSIGNED_BYTE}, nullptr);
		render->parameter(gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
		render->parameter(gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);

		auto texture = *render;
		
		auto depth = sbg::OwnedResource{sbg::RenderBuffer{}};
		depth->create();
		depth->storage(gl::GL_DEPTH_COMPONENT, {1366, 768});
		
		auto const framebuffer = std::make_shared<sbg::OwnedResource<sbg::FrameBuffer>>();
		
		(*framebuffer)->create();
		(*framebuffer)->attach(gl::GL_DEPTH_ATTACHMENT, std::move(depth));
		(*framebuffer)->attach(gl::GL_COLOR_ATTACHMENT0, std::move(render));
		
		sbg::Log::fatal(
			[&]{ return !(*framebuffer)->check(); },
			SBG_LOG_INFO,
			"Famebuffer", (*framebuffer)->id(), "invalid"
		);
		
		auto const program = std::make_shared<sbg::OwnedResource<sbg::Program>>(programs.create("postfx-test"));
		
		// TODO: We unwrap a owned resource, because there is actually nothing to own
		auto const mesh = meshes.create("fullscreen");
		
		return [mesh, &camera, texture, program, framebuffer, &mainEngine, time = 0.f, token = std::optional<sbg::OwnershipToken>{}](auto paint, sbg::UniformSet environment) mutable {
			if (!token) {
				token = mainEngine.add([&](sbg::Time delta) {
					time += delta.current * 1.5;
				});
			}
			
			using namespace sbg::literals;
			
			environment.emplace("view"_h, camera.view());
			environment.emplace("projection"_h, camera.projection());
			
			paint.prepare("default", environment);
			
			{
				(*framebuffer)->bind();
				gl::glViewport(0, 0, 1366, 768);
				gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);
				paint.draw("default");
				(*framebuffer)->draw(gl::GL_COLOR_ATTACHMENT0);
				(*framebuffer)->unbind();
			}
			
			(*program)->bind();
			gl::glActiveTexture(gl::GL_TEXTURE0);
			
			(*program)->uniformValue("texture", 0);
			(*program)->uniformValue("time", time);
			
			texture.bind();
			mesh.vao().bind();
			
			mesh.draw();
		};
	});
}
