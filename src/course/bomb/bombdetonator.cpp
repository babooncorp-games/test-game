#include "bombdetonator.h"

#include "subgine/audio/audiosystem.h"

#include "subgine/log.h"

using namespace course;

BombDetonator::BombDetonator(sbg::AudioSystem& audioSystem) noexcept :
	_audioSystem{audioSystem} {}


void BombDetonator::add(sbg::Entity bomb) {
	_bombs.emplace_back(bomb);
}

void BombDetonator::detonate() {
	if (!_bombs.empty()) {
		auto l = sbg::Log::trace(SBG_LOG_INFO, "Detonating bombs");
		_audioSystem.create_sound("explosion1").play();
		
		for (auto const& bomb : _bombs) {
			bomb.destroy();
		}
		
		_bombs.clear();
	}
	
}
