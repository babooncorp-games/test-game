#include "framesprite.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/utility/transformmodelmatrix.h"

using namespace course;
using namespace sbg::literals;

void FrameSprite::serialize(sbg::ModelUniformEncoder& uniforms, FrameSprite const& sprite) {
	uniforms
		<< sbg::uniform::resource("spritesheet"_h, sprite.texture)
		<< sbg::uniform::updating("model"_h, sbg::TransformModelMatrix<sbg::SimpleSprite>{})
		<< sbg::uniform::updating("frame"_h, sprite.frame)
		<< sbg::uniform::value("visible"_h, sprite.visible);
}
