#pragma once

#include "subgine/graphic/utility/uniformencoderforward.h"
#include "subgine/graphic/model/simplesprite.h"

namespace course {

struct FrameSprite : sbg::SimpleSprite {
	int frame = 0;
	unsigned int pressed = 0;
	
	static void serialize(sbg::ModelUniformEncoder& uniforms, FrameSprite const& sprite);
};

} // namespace course
