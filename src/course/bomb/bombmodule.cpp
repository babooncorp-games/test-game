#include "bombmodule.h"

#include "course/bomb.h"
#include "course/bomb/service.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/log.h"
#include "subgine/opengl.h"
#include "subgine/resource.h"
#include "subgine/system.h"

#include "subgine/opengl/service.h"
#include "subgine/system/service.h"

#include <chrono>

using namespace course;
using namespace std::literals;

void BombModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const {
	collisionReactorCreator.add("detonate", [](BombDetonator& detonator) {
		return [&detonator](sbg::BasicCollisionInfo) { detonator.detonate(); };
	});
	
	collisionReactorCreator.add("press", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity) {
		return [&scheduler, entity](sbg::BasicCollisionInfo) {
			auto& visual = entity.component<sbg::Visual>();
			
			// Each frame that a collision happen, increment sprite.pressed
			visual.visit("button", [](FrameSprite& sprite) {
				sprite.frame = 1;
				sprite.pressed++;
			});
			
			auto pressed = visual.inspect<FrameSprite>("button").pressed;
			
			scheduler.defer(50ms, [&visual, entity, pressed] {
				if (entity) {
					visual.visit("button", [&](FrameSprite& sprite) {
						// if sprite.press is the same value as captured, no collision happened in 50ms.
						if (pressed == sprite.pressed) {
							sprite.frame = 0;
						}
					});
				}
			});
		};
	});
}

void BombModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("detonator", [](BombDetonator& detonator, sbg::Entity bomb) {
		detonator.add(bomb);
	});
} 

void BombModule::setupModelCreator(sbg::ModelCreator& modelCreator) const {
	modelCreator.add("manual-frame-sprite", [](sbg::Entity entity, sbg::Property data) {
		FrameSprite sprite;
		auto const transform = sbg::Component<sbg::Transform>{entity};
		auto const origin = sbg::apply_transform_from_property(data);
		
		sprite.origin = origin;
		sprite.transform = transform;
		sprite.texture = sbg::TextureTag{data["texture"]};
		
		return sprite;
	});
}
