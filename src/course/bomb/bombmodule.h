#pragma once

namespace sbg {
	struct CollisionReactorCreator;
	struct EntityBindingCreator;
	struct ModelCreator;
}

namespace course {

struct BombModule {
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupModelCreator(sbg::ModelCreator& modelCreator) const;
};

} // namespace course
