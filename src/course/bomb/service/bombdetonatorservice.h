#pragma once

#include "../bombdetonator.h"

#include "subgine/audio/service/audiosystemservice.h"

#include "subgine/common/kangaru.h"

namespace course {

struct BombDetonatorService : kgr::single_service<BombDetonator, kgr::autowire> {};

auto service_map(const BombDetonator&) -> BombDetonatorService;

} // namespace course
