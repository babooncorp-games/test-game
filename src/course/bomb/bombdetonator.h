#pragma once

#include "subgine/entity/entity.h"

#include <vector>

namespace sbg {
	struct AudioSystem;
}

namespace course {

struct BombDetonator {
	explicit BombDetonator(sbg::AudioSystem& audioManager) noexcept;
	
	void add(sbg::Entity bomb);
	void detonate();
	
private:
	std::vector<sbg::Entity> _bombs;
	sbg::AudioSystem& _audioSystem;
};

} // namespace course
