#pragma once

namespace sbg {
	struct ActorCreator;
	struct EntityBindingCreator;
	struct PluggerCreator;
}

namespace course {

struct PlayerModule {
	void setupActorCreator(sbg::ActorCreator& actorCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
};

} // namespace course
