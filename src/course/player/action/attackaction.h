#pragma once

#include <string>
#include <optional>

namespace course {

struct AttackAction {
	std::optional<std::string> projectile = {};
};

} // namespace course
