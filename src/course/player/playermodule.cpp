#include "playermodule.h"

#include "course/player.h"
#include "course/player/service.h"

#include "subgine/entity.h"
#include "subgine/actor.h"
#include "subgine/log.h"
#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/scene.h"

#include "subgine/entity/service.h"
#include "subgine/event/service.h"

using namespace course;

void PlayerModule::setupActorCreator(sbg::ActorCreator& actorCreator) const {
	actorCreator.add("course-attack-projectile", [](sbg::EntityFactory factory, sbg::Entity entity, sbg::Property data) {
		auto const default_type = std::string{data["default-projectile"]};
		auto const physic = sbg::Component<sbg::PhysicPoint2D>{entity};
		
		return [factory, default_type, physic](AttackAction action) {
			if (not physic) return;
			auto projectile = factory.create(action.projectile.value_or(default_type));
			auto physicProjectile = sbg::Component<sbg::PhysicPoint2D>{projectile};
			physicProjectile->setPosition(physic->getPosition());
			physicProjectile->setVelocity(physic->getVelocity() / 2);
		};
	});
}

void PlayerModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("block-controls", [](Controls& controls, sbg::Entity entity) {
		controls.control(entity);
	});
}

void PlayerModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("course-controls", [](sbg::EventDispatcher& events, Controls& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}
