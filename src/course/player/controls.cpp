#include "controls.h"

#include "action/attackaction.h"

#include "subgine/actor.h"
#include "subgine/window.h"
#include "subgine/actor-physic.h"
#include "subgine/log.h"

using namespace course;

Controls::Controls(sbg::InputTracker& tracker) : _tracker{tracker} {}

void Controls::control(sbg::Entity entity) {
	sbg::Log::trace(SBG_LOG_INFO, "Entity set to controls");
	_target = entity;
}

void Controls::handle(const sbg::KeyboardEvent& event) {
	if (_target) {
		auto& actor = _target.component<sbg::Actor>();
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::Left || event.keyCode == sbg::KeyboardEvent::KeyCode::Right) {
			auto movement = sbg::Vector2d{};
			
			movement.x -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Left);
			movement.x += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Right);
			
			actor.act(sbg::Move2DAction{movement});
		}
		
		if (event.pressed && event.keyCode == sbg::KeyboardEvent::KeyCode::X) {
			actor.act(AttackAction{});
		}
		
		if (event.pressed && event.keyCode == sbg::KeyboardEvent::KeyCode::C) {
			actor.act(AttackAction{"course-big-bomb"});
		}
		
		if (event.pressed && event.keyCode == sbg::KeyboardEvent::KeyCode::Z) {
			actor.act(sbg::JumpAction{});
		}
	}
}
