#pragma once

#include "subgine/common/kangaru.h"

#include "../assetmodule.h"

namespace course {

struct AssetModuleService : kgr::single_service<AssetModule>,
	sbg::autocall<
	> {};

auto service_map(const AssetModule&) -> AssetModuleService;

} // namespace course

