#version 140

out vec4 outColor;
in vec2 UV;

uniform sampler2D tex;
uniform float time;

void main() {
	outColor = vec4( texture(tex, UV + vec2(0, sin(time + UV.x * 100)) / 100).xyz, 1);
}
