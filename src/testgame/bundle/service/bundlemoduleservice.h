#pragma once

#include "../bundlemodule.h"

#include "subgine/common/kangaru.h"

namespace testgame {

struct BundleModuleService : kgr::single_service<BundleModule>,
	sbg::autocall<
	> {};

auto service_map(const BundleModule&) -> BundleModuleService;

} // namespace testgame 
