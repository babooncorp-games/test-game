Type | Name | Source
--- | --- | ---
Tileset | floating-dungeon | https://opengameart.org/content/dungeon-tileset
Tileset | floating-islands | https://opengameart.org/content/the-field-of-the-floating-islands
Sprite | roguelike-bosses | https://opengameart.org/content/roguelike-bosses
Sprite | roguelike-creatures | https://opengameart.org/content/roguelike-monsters
