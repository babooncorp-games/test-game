subgine_build_assets(bundle-assets
	ASSET bundle
		TYPE json
		SOURCES
			aseprite.json
			texture.json
			tileset.json
	
	ASSET bundle/blaster-suit/texture/tileset/sci-fi-ruins
		TYPE tileset
		SOURCES blaster-suit/texture/tileset/sci-fi-ruins.png
		OPTIONS 48 48
	
	ASSET bundle/blaster-suit/texture/sprite/object/energy-tank
		TYPE spritesheet
		SOURCES blaster-suit/texture/sprite/object/energy-tank.png
		OPTIONS 48 48
			
	ASSET bundle/blaster-suit/texture/sprite/monster/gear-spider
		TYPE spritesheet
		SOURCES blaster-suit/texture/sprite/monster/gear-spider.png
		OPTIONS 48 48
		
	ASSET bundle/blaster-suit/texture/sprite/object/energy-tank
		TYPE json
		SOURCES blaster-suit/texture/sprite/object/energy-tank.json
	
	ASSET bundle/blaster-suit/texture/sprite/monster/gear-spider
		TYPE json
		SOURCES blaster-suit/texture/sprite/monster/gear-spider.json
	
	ASSET bundle/evolution/texture/sprite/embryo
		TYPE file
		SOURCES evolution/texture/sprite/embryo.png
	
	ASSET bundle/maya/texture/tileset/32x32/maya
		TYPE tileset
		SOURCES maya/texture/tileset/32x32/maya.png
		OPTIONS 32 32
	
	ASSET bundle/maya/texture/background/blue-mountains
		TYPE file
		SOURCES maya/texture/background/blue-mountains.png
	
	ASSET bundle/floating-islands/texture/tileset/32x32/floating-islands
		TYPE tileset
		SOURCES floating-islands/texture/tileset/32x32/floating-islands.png
		OPTIONS 32 32
		
	ASSET bundle/floating-islands/texture/background/gray-void
		TYPE file
		SOURCES floating-islands/texture/background/gray-void.png
	
	ASSET bundle/simples-pimbles/texture/tileset/16x16/simple
		TYPE tileset
		SOURCES simples-pimbles/texture/tileset/16x16/simple.png
		OPTIONS 16 16
		
	ASSET bundle/generic/texture/background/country-platform-back
		TYPE file
		SOURCES generic/texture/background/country-platform-back.png
	
	ASSET bundle/simples-pimbles/texture/sprite/hero1
		TYPE spritesheet
		SOURCES simples-pimbles/texture/sprite/hero1.png
		OPTIONS 16 16
	
	ASSET bundle/simples-pimbles/texture/sprite/hero1
		TYPE json
		SOURCES simples-pimbles/texture/sprite/hero1.json
	
	ASSET bundle/simples-pimbles/texture/sprite/projectile1
		TYPE spritesheet
		SOURCES simples-pimbles/texture/sprite/projectile1.png
		OPTIONS 16 16
	
	ASSET bundle/simples-pimbles/texture/sprite/projectile1
		TYPE json
		SOURCES simples-pimbles/texture/sprite/projectile1.json
	
	ASSET bundle/golem/texture/sprite/BigHat
		TYPE spritesheet
		SOURCES golem/texture/sprite/BigHat.png
		OPTIONS 32 32
	
	ASSET bundle/golem/texture/sprite/BigHat
		TYPE json
		SOURCES golem/texture/sprite/BigHat.json
	
	ASSET bundle/golem/texture/tileset/sandbox
		TYPE tileset
		SOURCES golem/texture/tileset/sandbox.png
		OPTIONS 32 32
	
	ASSET bundle/generic/texture/background/sunset
		TYPE file
		SOURCES generic/texture/background/parallax-mountain-bg.png
		
	ASSET bundle/thieve-and-king/texture/tileset/thieve-and-king
		TYPE tileset
		SOURCES thieve-and-king/texture/tileset/16x16/thieve-and-king-improved.png
		OPTIONS 16 16
	
	ASSET bundle/thieve-and-king/texture/sprite/snek
		TYPE spritesheet
		SOURCES thieve-and-king/texture/sprite/16x16/snek.png
		OPTIONS 32 32
		
	ASSET bundle/evolution/texture/background/testbackground
		TYPE file
		SOURCES evolution/texture/background/testbackground.png
		
	ASSET bundle/evolution/texture/sprite/runner
		TYPE spritesheet
		SOURCES evolution/texture/sprite/runner.png
		OPTIONS 32 32

	ASSET bundle/thieve-and-king/texture/sprite/snek-animation
		TYPE json
		SOURCES thieve-and-king/texture/sprite/16x16/snek.json
		
	ASSET bundle/evolution/texture/sprite/runner
		TYPE json
		SOURCES evolution/texture/sprite/runner.json
		
	ASSET bundle/evolution/texture/tileset/ruins
		TYPE tileset
		SOURCES evolution/texture/tileset/ruins.png
		OPTIONS 32 32
		
	ASSET bundle/tiny-adventure-pack/texture/tileset/adventure
		TYPE tileset
		SOURCES tiny-adventure-pack/texture/tileset/adventure.png
		OPTIONS 16 16
		
	ASSET bundle/tiny-adventure-pack/texture/tileset/collisions
		TYPE tileset
		SOURCES tiny-adventure-pack/texture/tileset/collisions.png
		OPTIONS 16 16
		
	ASSET bundle/tiny-adventure-pack/texture/sprite/skeleton
		TYPE spritesheet
		SOURCES tiny-adventure-pack/texture/sprite/skeleton.png
		OPTIONS 16 16
		
	ASSET bundle/tiny-adventure-pack/texture/sprite/skeleton-animation
		TYPE json
		SOURCES tiny-adventure-pack/texture/sprite/skeleton.json
		
	ASSET bundle/tiny-adventure-pack/texture/sprite/rock
		TYPE spritesheet
		SOURCES tiny-adventure-pack/texture/sprite/rock.png
		OPTIONS 16 16
		
	ASSET bundle/tiny-adventure-pack/texture/sprite/rock-animation
		TYPE json
		SOURCES tiny-adventure-pack/texture/sprite/rock.json
		
	ASSET bundle-assets
		TYPE mapping
		SOURCES
			aseprite.json
			texture.json
)