Type | Name | Source
--- | --- | ---
Background | country-platform | https://opengameart.org/content/country-side-platform-tiles
Background | parallax-mountain | https://opengameart.org/content/mountain-at-dusk-background
Background | ruined-city | https://opengameart.org/content/ruined-city-background
Sprite | hud-pieces | https://opengameart.org/content/shiny-hud
Sprite | oceanicons | https://opengameart.org/content/oceans-icons
Sprite | shiny-potions | https://opengameart.org/content/shiny-rpg-potions-16x16
