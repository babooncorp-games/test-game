#pragma once

#include "subgine/entity/entity.h"

namespace testgame::testbag {

struct Hero {
	sbg::Entity entity;
};

} // namespace testgame::testbag
