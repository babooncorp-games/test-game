#include "testbagmodule.h"

#include "testgame/test-bag.h"
#include "testgame/test-bag/service.h"

#include "subgine/bag.h"
#include "subgine/entity.h"
#include "subgine/resource.h"
#include "subgine/scene.h"

using namespace testgame::testbag;

void TestBagModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("hero", [](Hero& hero, sbg::Entity entity) {
		hero.entity = entity;
	});

	entityBindingCreator.add("2dflat-controls", [](Controls& controls, sbg::Entity entity) {
		controls.control(entity);
	});
}

void TestBagModule::setupEffectCreator(sbg::EffectCreator& effectCreator) const {
	effectCreator.add("test", [](sbg::Property data){
		return Test{};
	});
}

void TestBagModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("testbag-controls", [](sbg::EventDispatcher& events, Controls& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}
