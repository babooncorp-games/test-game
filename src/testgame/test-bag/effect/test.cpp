#include "test.h"

namespace testgame::testbag {

bool Test::apply(sbg::Entity target) {
	bool success = false;

	if (target.alive()) {
		target.destroy();
		success = true;
	}

	return success;
}

} // namespace testgame::testbag
