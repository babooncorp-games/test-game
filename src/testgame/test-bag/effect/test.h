#pragma once

#include "subgine/entity/entity.h"

namespace testgame::testbag {

struct Test {
	bool apply(sbg::Entity target);
};

} // namespace testgame::testbag
