#pragma once

#include "subgine/common/kangaru.h"

#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/bag/service/effectcreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"

#include "../testbagmodule.h"

namespace testgame::testbag {

struct TestBagModuleService : kgr::single_service<TestBagModule>,
	sbg::autocall<
		&TestBagModule::setupEntityBindingCreator,
		&TestBagModule::setupEffectCreator,
		&TestBagModule::setupPluggerCreator
	> {};

auto service_map(const TestBagModule&) -> TestBagModuleService;

} // namespace testgame::testbag
