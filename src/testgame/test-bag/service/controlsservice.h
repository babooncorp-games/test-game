#pragma once

#include "subgine/common/kangaru.h"

#include "../controls.h"

#include "subgine/window/service/inputtrackerservice.h"

namespace testgame::testbag {

struct ControlsService : kgr::single_service<Controls, kgr::autowire> {};

auto service_map(Controls const&) -> ControlsService;

} // namespace testgame::testbag
