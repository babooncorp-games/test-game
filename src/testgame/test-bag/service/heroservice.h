#pragma once

#include "subgine/common/kangaru.h"

#include "../hero.h"

namespace testgame::testbag {

struct HeroService : kgr::single_service<Hero> {};

auto service_map(const Hero&) -> HeroService;

} // namepace testgame::testbag
