#include "controls.h"

#include "subgine/actor.h"
#include "subgine/actor-physic.h"
#include "subgine/log.h"
#include "subgine/window.h"

using namespace testgame::testbag;

Controls::Controls(sbg::InputTracker& tracker) : _tracker{tracker} {
}

void Controls::control(sbg::Entity entity) {
	sbg::Log::trace(SBG_LOG_INFO, "Entity set to controls, instance", this);
	_target = entity;
}

void Controls::handle(const sbg::KeyboardEvent& event) {
	if (_target) {
		auto& actor = _target.component<sbg::Actor>();
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::Left ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Right ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Up ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Down) {
			auto movement = sbg::Vector2d{};
			
			movement.x -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Left);
			movement.x += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Right);
			movement.y -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Up);
			movement.y += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Down);
			
			actor.act(sbg::Move2DAction{movement});
		}
	}
}
