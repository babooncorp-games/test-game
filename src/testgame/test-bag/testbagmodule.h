#pragma once

namespace sbg {
	struct EntityBindingCreator;
	struct EffectCreator;
	struct PluggerCreator;
} // namespace sbg

namespace testgame::testbag {

struct TestBagModule {
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupEffectCreator(sbg::EffectCreator& effectCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
};

} // namespace testgame::testbag
