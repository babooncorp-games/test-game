#include "cameracontroller.h"

#include "subgine/log.h"

using namespace testgame::testaseprite;

CameraController::CameraController(sbg::InputTracker& tracker) : _tracker{tracker} {}

void CameraController::handle(const sbg::KeyboardEvent& event) {
	if (event.keyCode == sbg::KeyboardEvent::KeyCode::Left ||
		event.keyCode == sbg::KeyboardEvent::KeyCode::Right ||
		event.keyCode == sbg::KeyboardEvent::KeyCode::Up ||
		event.keyCode == sbg::KeyboardEvent::KeyCode::Down) {
		auto movement = sbg::Vector2d{};
		
		movement.x -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Left);
		movement.x += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Right);
		movement.y -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Up);
		movement.y += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Down);
		
		position.x += -(movement.x * 32);
		position.y += -(movement.y * 32);
		sbg::Log::trace(SBG_LOG_INFO, "X: ", sbg::log::enquote(position.x));
		sbg::Log::trace(SBG_LOG_INFO, "Y: ", sbg::log::enquote(position.y));
	} else if (event.keyCode == sbg::KeyboardEvent::KeyCode::PageUp ||
		event.keyCode == sbg::KeyboardEvent::KeyCode::PageDown) {
		if (_tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::PageUp)) {
			height += 32;
		}
		if (_tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::PageDown)) {
			height -= 32;
		}
	}
	
}
