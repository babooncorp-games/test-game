#include "testasepritemodule.h"

#include "testgame/test-aseprite.h"
#include "testgame/test-aseprite/service.h"

#include "subgine/entity.h"
#include "subgine/provider.h"
#include "subgine/graphic.h"
#include "subgine/window.h"
#include "subgine/vector.h"
#include "subgine/scene.h"

#include "subgine/entity/service.h"
#include "subgine/provider/service.h"
#include "subgine/graphic/service.h"
#include "subgine/window/service.h"
#include "subgine/scene/service.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace testgame::testaseprite;

void TestAsepriteModule::setupProviderCreator(sbg::ProviderCreator<glm::mat4>& providerCreator) const {
	providerCreator.add("controlled-view", [](CameraController& cameraController, sbg::Entity, sbg::Property data) {
		auto position = sbg::Vector3f{data["position"]};
		cameraController.position = position;
		return [&cameraController] { return glm::translate(glm::mat4{1.f}, glm::vec3{cameraController.position.x, cameraController.position.y, cameraController.position.z}); };
	});

	providerCreator.add("controlled-projection", [](CameraController& cameraController, sbg::Window& window, sbg::Entity entity, sbg::Property data) {
		auto height = float{data["height"]};
		cameraController.height = height;
		
		return [&window, &cameraController] {
			auto size = window.size();
			
			return glm::ortho(
				(size.x * (cameraController.height / size.y)) / -2, (size.x * (cameraController.height / size.y)) / 2,
				cameraController.height / 2, cameraController.height / -2,
				-1.f, 100.f
			);
		};
	});
}

void TestAsepriteModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("camera-controls", [](sbg::EventDispatcher& events, CameraController& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}
