#pragma once

#include "../cameracontroller.h"

#include "subgine/window/service/inputtrackerservice.h"

#include "subgine/common/kangaru.h"

namespace testgame::testaseprite {

struct CameraControllerService : kgr::single_service<CameraController, kgr::autowire> {};

auto service_map(const CameraController&) -> CameraControllerService;

} // namepace testgame::testaseprite
