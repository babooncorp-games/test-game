#pragma once

#include "../testasepritemodule.h"

#include "subgine/provider/service/providercreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace testgame::testaseprite {

struct TestAsepriteModuleService : kgr::single_service<TestAsepriteModule>,
	sbg::autocall<
		&TestAsepriteModule::setupProviderCreator,
		&TestAsepriteModule::setupPluggerCreator
	> {};

auto service_map(TestAsepriteModule const&) -> TestAsepriteModuleService;

} // namespace testgame::testaseprite
