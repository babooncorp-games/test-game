#pragma once

#include <glm/glm.hpp>

namespace sbg {
	template<typename>
	struct ProviderCreator;
	struct PluggerCreator;
} // namespace sbg

namespace testgame::testaseprite {

struct TestAsepriteModule {
	void setupProviderCreator(sbg::ProviderCreator<glm::mat4>& providerCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
};

} // namespace testgame::testaseprite
