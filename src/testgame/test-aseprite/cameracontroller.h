#pragma once


#include "subgine/window/inputtracker.h"
#include "subgine/window/event/keyboardevent.h"
#include "subgine/vector/vector.h"

namespace testgame::testaseprite {

struct CameraController {
	CameraController(sbg::InputTracker& tracker);
	void handle(const sbg::KeyboardEvent& event);

	float height;
	sbg::Vector3f position;
	
private:
	sbg::InputTracker& _tracker;
};

} // namespace testgame::testaseprite
