#include "spaceshootermodule.h"

#include "testgame/space-shooter.h"
#include "testgame/space-shooter/service.h"

#include "subgine/animation.h"
#include "subgine/scene.h"
#include "subgine/entity.h"
#include "subgine/event.h"
#include "subgine/state.h"
#include "subgine/collision.h"
#include "subgine/graphic.h"
#include "subgine/system.h"
#include "subgine/physic.h"
#include "subgine/tiled.h"
#include "subgine/resource.h"

#include "subgine/animation/service.h"
#include "subgine/resource/service.h"
#include "subgine/opengl/service.h"
#include "subgine/audio/service.h"
#include "subgine/collision/service.h"
#include "subgine/event/service.h"
#include "subgine/tiled/service.h"
#include "subgine/system/service.h"

#include <cmath>

using namespace testgame::spaceshooter;
using namespace std::literals;

void SpaceShooterModule::setupComponentCreator(sbg::ComponentCreator& componentCreator) const {
	componentCreator.add("spaceship-control", [] { return sbg::StateMachine{Idle{}}; });
	componentCreator.add("spaceship", [](sbg::Entity entity, sbg::Property data) {
		auto loadGun = [](sbg::Property data) {
			Gun gun;
			
			gun.accuracy = float{data["accuracy"]};
			gun.bullets = data["bullets"];
			gun.cooldown = std::chrono::microseconds{static_cast<std::int64_t>(float{data["cooldown"]} * 1000)};
			gun.initialDelay = std::chrono::milliseconds{std::int64_t{data["delay"]}};
			gun.offset = sbg::Vector2f{data["offset"]};
			gun.mirroired = bool{data["mirroired"]};
			
			return gun;
		};
		
		Ship ship;
		
		ship.mainWeapon = loadGun(data["mainWeapon"]);
		
		if (data["secondaryWeapon"].isObject()) {
			ship.secondaryWeapon = loadGun(data["secondaryWeapon"]);
		}
		
		for (auto const gunData : data["otherWeapons"]) {
			ship.otherWeapons.emplace_back(loadGun(gunData));
		}
		
		return ship;
	});
	
	componentCreator.add("enemy-spawner", [](sbg::Entity entity) {
		auto tiled = sbg::Component<sbg::tiled::Data>{entity};
		auto const& properties = tiled->object->properties;
		
		return EnemySpawner{
			properties.get<std::string>("load-type"),
			properties.get<std::string>("path"),
			properties.get_or("loop", ""s),
			properties.get_or("shooting-loop", ""s),
			properties.get<float>("duration")
		};
	});
	
	componentCreator.add("enemy", [](sbg::AnimationEngine& animationEngine, sbg::GameDatabase& database, sbg::Entity entity) {
		sbg::Log::fatal([&]{ return !entity.has<EnemySpawner>(); }, SBG_LOG_INFO, "Enemy component need to read EnemySpawner");
		auto& data = entity.component<EnemySpawner>();
		
		auto load_path = [](auto& keyFrames, sbg::Property data, auto parse) {
			for (auto const frame : data["targets"]) {
				keyFrames.emplace_back(parse(frame["value"]), double{frame["until"]});
			}
		};
		
		auto pathKeyFrames = std::vector<sbg::Keyframe<sbg::Vector2d>>{};
		auto pathJsonData = database.get("path", data.path);
		load_path(pathKeyFrames, pathJsonData,
			[](auto const vec) { return sbg::Vector2d{vec}; }
		);
		
		auto pathAnimation = animationEngine.add(
			std::move(pathKeyFrames),
			sbg::Linear{},
			sbg::Clamp{},
			[entity](sbg::Vector2d mixedValue) {
				if (!entity) return;
				auto const physic = sbg::Component<sbg::PhysicPoint2D>{entity};
				auto const enemy = sbg::Component<Enemy>{entity};
				if (enemy->path.cursor() == 1) {
					if (enemy->loop.valid()) {
						enemy->path.stop();
						enemy->loop.start();
					} else {
						entity.destroy();
					}
				} else {
					physic->setPosition(mixedValue + sbg::Vector2d{enemy->offset - (1920 / 2), 0});
				}
			}
		);
		
		auto loopAnimation = sbg::AnimationEngine::AnimationHandle{};
		
		if (!data.loop.empty()) {
			auto loopKeyFrames = std::vector<sbg::Keyframe<sbg::Vector2d>>{};
			auto loopJsonData = database.get("path", data.loop);
			load_path(loopKeyFrames, loopJsonData,
				[](auto const vec) { return sbg::Vector2d{vec}; }
			);
			
			loopAnimation = animationEngine.add(
				std::move(loopKeyFrames),
				sbg::Linear{},
				sbg::Repeat{},
				[entity](sbg::Vector2d mixedValue) {
					if (!entity) return;
					auto const physic = sbg::Component<sbg::PhysicPoint2D>{entity};
					auto const enemy = sbg::Component<Enemy>{entity};
					physic->setPosition(mixedValue + sbg::Vector2d{enemy->offset - (1920 / 2), 0});
				}
			);
		}
		
		auto fireringKeyFrames = std::vector<sbg::Keyframe<bool>>{};
		auto fireringJsonData = database.get("path", data.shooting);
		load_path(fireringKeyFrames, fireringJsonData,
			[](auto const shooting) { return bool{shooting}; }
		);
		
		auto fireringAnimation = animationEngine.add(
			std::move(fireringKeyFrames),
			sbg::Nearest{},
			sbg::Repeat{},
			[entity](bool mixedValue) {
				if (!entity) return;
				auto& ship = entity.component<Ship>();
				if (mixedValue && !ship.fires()) {
					ship.startFire();
				} else if (!mixedValue && ship.fires()) {
					ship.stopFire();
				}
			}
		);
		
		auto enemy = Enemy{
			0,
			pathAnimation,
			loopAnimation,
			fireringAnimation
		};
		
		enemy.path.start();
		enemy.firering.start();
		
		return enemy;
	});
	
	componentCreator.add("health", [](sbg::Entity entity, sbg::Property data) {
		return Health{
			int{data["health"]},
			int{data["shield"]},
			int{data["max-health"]},
			int{data["max-shield"]},
			int{data["max-damage"]},
			std::string{data["death"]},
			std::chrono::milliseconds{std::int64_t{data["delay"]}},
		};
	});
}

void SpaceShooterModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("shooting", [](ShootingEngine& engine, sbg::Entity entity) {
		engine.add(entity);
	});
	
	entityBindingCreator.add("spaceship-control", [](ShipControl& control, sbg::Entity entity) {
		control.control(entity);
	});
	
	entityBindingCreator.add("spawner", [](EnemyEngine& enemies, sbg::Entity entity) {
		enemies.add(entity);
	});
	
	entityBindingCreator.add("remove", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity, sbg::Property data) {
		scheduler.defer(
			std::chrono::milliseconds{std::int64_t{data["lifetime"]}},
			[entity] {
				entity.destroy();
			}
		);
	});
}

void SpaceShooterModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("ship-controls", [](ShipControl& controls, sbg::EventDispatcher& eventDispatcher) {
		return sbg::Plugger{
			[&]{ eventDispatcher.subscribe(controls); },
			[&]{ eventDispatcher.unsubscribe(controls); }
		};
	});
}

void SpaceShooterModule::setupSceneCreator(sbg::SceneCreator& sceneCreator) const {
	sceneCreator.add("spaceshooter-level", [](sbg::tiled::MapFactory tiledMapFactory, sbg::tiled::MapMaterializer materializer, sbg::Property data) {
		return [tiledMapFactory, materializer, name = std::string{data["name"]}] {
			auto map = tiledMapFactory.create(name);
			materializer.materialize(map);
		};
	});
}

void SpaceShooterModule::setupCollisionProfileCreator(sbg::CollisionProfileCreator& profileCreator) const {
	profileCreator.add("inside-out-square", [](sbg::CollisionReactorFactory reactorFactory, sbg::Entity entity, sbg::Property data) {
		return InsideoutSquareAspect{
			sbg::Vector{double{data["width"]}, double{data["height"]}} * -0.5,
			sbg::Vector{double{data["width"]}, double{data["height"]}} * 0.5
		};
	});
}

void SpaceShooterModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& reactorCreator) const {
	reactorCreator.add("health", [](sbg::EntityFactory entityFactory, sbg::EntityManager& entities, sbg::AudioSystem& audioSystem, sbg::CollisionEngine& collisions, ShootingEngine& shooting, sbg::DeferredScheduler& deferred, sbg::Entity entity, sbg::Property) {
		return [entity, entityFactory, &entities, &deferred, &collisions, &shooting, &audioSystem](const sbg::BasicCollisionInfo& info) {
			sbg::Log::fatal([&]{ return !entity.has<Health>(); }, SBG_LOG_INFO, "Entity health colliding need an health component");
			sbg::Log::fatal([&]{ return !info.other.has<Health>(); }, SBG_LOG_INFO, "Health colliding with an entity that don't have health");
			
			auto& health_us = entity.component<Health>();
			auto const& health_other = info.other.component<Health>();
			
			auto damage = std::min(health_other.last_health + health_other.last_shield, health_other.max_damage);
			health_us.save_last();
			
			health_us.shield -= damage;
			
			if (health_us.shield < 0) {
				health_us.health += health_us.shield;
				health_us.shield = 0;
			}
			
			if (health_us.health < 0) {
				health_us.health = 0;
				health_us.dead = true;
				collisions.remove(entity);
				shooting.remove(entity);
				
				if (!health_us.death.empty()) {
					auto death = entities.create();
					
					audioSystem.create_sound("explosion1").play();
					entityFactory.create(health_us.death, death);
					
					auto const deathPhysic = sbg::Component<sbg::PhysicBody2D>{death};
					auto const entityPhysic = sbg::Component<sbg::PhysicBody2D>{entity};
					
					if (deathPhysic and entityPhysic) {
						deathPhysic->setPosition(deathPhysic->getPosition() + entityPhysic->getPosition());
					}
				}
				
				deferred.defer(health_us.delay, [entity] {
					sbg::Entity{entity}.destroy();
				});
			}
		};
	});
}

void SpaceShooterModule::setupCollisionEngine(sbg::CollisionEngine& collisionEngine) const {
	collisionEngine.addTester<sbg::AlignedBox2DAspect, InsideoutSquareAspect>(AlignedBoxInsideAlgorithm{});
}
