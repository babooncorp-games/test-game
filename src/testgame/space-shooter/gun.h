#pragma once

#include <chrono>
#include <string>

#include "subgine/vector/vector.h"

namespace testgame::spaceshooter {

/**
 * This is a gun attached to a ship
 */
struct Gun {
	using time_point = std::chrono::high_resolution_clock::time_point;
	
	float accuracy;
	std::string bullets;
	std::chrono::microseconds cooldown;
	std::chrono::milliseconds initialDelay{0};
	time_point lastShot{time_point::min()};
	time_point startsShooting{time_point::min()};
	sbg::Vector2f offset;
	bool mirroired;
};

} // namespace testgame::spaceshooter
