#pragma once

namespace testgame::spaceshooter {

struct MovingDown {};
struct MovingLeft {};
struct MovingRight {};
struct MovingUp {};

} // namespace testgame::spaceshooter
