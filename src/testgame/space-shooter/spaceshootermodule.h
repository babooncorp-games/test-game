#pragma once

namespace sbg {
	struct ComponentCreator;
	struct EntityBindingCreator;
	struct PluggerCreator;
	struct SceneCreator;
	struct CollisionProfileCreator;
	struct CollisionReactorCreator;
	struct CollisionEngine;
}

namespace testgame::spaceshooter {
	
struct SpaceShooterModule {
	void setupComponentCreator(sbg::ComponentCreator& componentCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
	void setupSceneCreator(sbg::SceneCreator& sceneCreator) const;
	void setupCollisionProfileCreator(sbg::CollisionProfileCreator& profileCreator) const;
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& reactorCreator) const;
	void setupCollisionEngine(sbg::CollisionEngine& collisionEngine) const;
};

} // namespace testgame::spaceshooter
