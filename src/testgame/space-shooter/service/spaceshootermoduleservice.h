#pragma once

#include "subgine/common/kangaru.h"

#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/collision/service/collisionengineservice.h"
#include "subgine/scene/service/scenecreatorservice.h"

#include "../spaceshootermodule.h"

namespace testgame::spaceshooter {

struct SpaceShooterModuleService : kgr::single_service<SpaceShooterModule>,
	sbg::autocall<
		&SpaceShooterModule::setupEntityBindingCreator,
		&SpaceShooterModule::setupComponentCreator,
		&SpaceShooterModule::setupPluggerCreator,
		&SpaceShooterModule::setupSceneCreator,
		&SpaceShooterModule::setupCollisionProfileCreator,
		&SpaceShooterModule::setupCollisionReactorCreator,
		&SpaceShooterModule::setupCollisionEngine
	> {};

auto service_map(const SpaceShooterModule&) -> SpaceShooterModuleService;

} // namespace testgame::spaceshooter
