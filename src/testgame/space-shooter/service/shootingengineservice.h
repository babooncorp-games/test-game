#pragma once

#include "../engine/shootingengine.h"

#include "subgine/system/service/mainengineservice.h"
#include "subgine/entity/service/entitycreatorservice.h"
#include "subgine/common/kangaru.h"

namespace testgame::spaceshooter {

struct ShootingEngineService : kgr::single_service<ShootingEngine, kgr::dependency<sbg::EntityFactoryService>>,
	sbg::autocall<&ShootingEngine::setupMainEngine> {};

auto service_map(ShootingEngine&) -> ShootingEngineService;

} // namespace testgame::spaceshooter
