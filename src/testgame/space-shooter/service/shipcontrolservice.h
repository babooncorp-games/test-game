#pragma once

#include "../shipcontrol.h"
#include "subgine/window/service/inputtrackerservice.h"
#include "subgine/common/kangaru.h"

namespace testgame::spaceshooter {

struct ShipControlService : kgr::single_service<ShipControl, kgr::dependency<sbg::InputTrackerService>> {};

auto service_map(const ShipControl&) -> ShipControlService;

} // namespace testgame::spaceshooter
