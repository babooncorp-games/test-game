#pragma once

#include "subgine/common/kangaru.h"
#include "../engine/enemyengine.h"

#include "subgine/entity/service/entitycreatorservice.h"
#include "subgine/entity/service/entitymanagerservice.h"
#include "subgine/system/service/mainengineservice.h"

namespace testgame::spaceshooter {

struct EnemyEngineService : kgr::single_service<EnemyEngine, kgr::dependency<sbg::EntityFactoryService, sbg::EntityManagerService>>,
	sbg::autocall<&EnemyEngine::setupMainEngine> {};

auto service_map(const EnemyEngine&) -> EnemyEngineService;

} // namespace testgame::spaceshooter
