#include "shipcontrol.h"

#include "component/ship.h"

#include "state/idle.h"
#include "state/moving.h"

#include "subgine/event.h"
#include "subgine/window.h"
#include "subgine/state.h"
#include "subgine/physic.h"

using namespace testgame::spaceshooter;

ShipControl::ShipControl(sbg::InputTracker& inputs) noexcept : _inputs{inputs} {}

void ShipControl::control(sbg::Entity ship) {
	_ship = ship;
}

void ShipControl::handle(const sbg::KeyboardEvent& event) {
	if (_ship) {
		auto const physic = sbg::Component<sbg::PhysicPoint2D>(_ship);
		auto const states = sbg::Component<sbg::StateMachine>(_ship);
		auto const ship = sbg::Component<Ship>(_ship);
		
		static constexpr auto moveValue = 600000.;
		
		auto moveLeft = sbg::Transition{
			[physic](Idle) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1} + sbg::Vector2d{-moveValue, 0});
				return MovingLeft{};
			},
			[physic](MovingRight) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1} + sbg::Vector2d{-moveValue, 0});
				return MovingLeft{};
			}
		};
		
		auto moveRight = sbg::Transition{
			[physic](Idle) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1} + sbg::Vector2d{moveValue, 0});
				return MovingRight{};
			},
			[physic](MovingLeft) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1} + sbg::Vector2d{moveValue, 0});
				return MovingRight{};
			}
		};
		
		auto moveUp = sbg::Transition{
			[physic](Idle) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0} + sbg::Vector2d{0, -moveValue});
				return MovingUp{};
			},
			[physic](MovingDown) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0} + sbg::Vector2d{0, -moveValue});
				return MovingUp{};
			}
		};
		
		auto moveDown = sbg::Transition{
			[physic](Idle) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0} + sbg::Vector2d{0, moveValue});
				return MovingDown{};
			},
			[physic](MovingUp) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0} + sbg::Vector2d{0, moveValue});
				return MovingDown{};
			}
		};
		
		auto stop = sbg::Transition{
			[physic](MovingRight) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1});
				return Idle{};
			},
			[physic](MovingUp) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0});
				return Idle{};
			},
			[physic](MovingDown) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{1, 0});
				return Idle{};
			},
			[physic](MovingLeft) {
				physic->setForce("move", physic->getForce("move") * sbg::Vector2d{0, 1});
				return Idle{};
			}
		};
		
		if (
			event.keyCode == sbg::KeyboardEvent::KeyCode::Left ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Right
		) {
			if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Left) && !states->at<MovingLeft>()) {
				states->transit(moveLeft);
			} else if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Right) && !states->at<MovingRight>()) {
				states->transit(moveRight);
			} else if (!event.pressed && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Left) && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Right)) {
				states->transit(stop);
			}
		} else if (
			event.keyCode == sbg::KeyboardEvent::KeyCode::Up ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Down
		) {
			if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Up) && !_states.at<MovingUp>()) {
				_states.transit(moveUp);
			} else if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Down) && !_states.at<MovingDown>()) {
				_states.transit(moveDown);
			} else if (!event.pressed && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Down) && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Up)) {
				_states.transit(stop);
			}
		} else if (
			event.keyCode == sbg::KeyboardEvent::KeyCode::Space
		) {
			event.pressed ? ship->startFire() : ship->stopFire();
		}
	}
}
