#pragma once

#include "state/idle.h"

#include "subgine/window/event/keyboardevent.h"
#include "subgine/entity/entity.h"
#include "subgine/state/statemachine.h"

namespace sbg {
	struct InputTracker;
}

namespace testgame::spaceshooter {

/**
 * Used to handle key events to controls a particular ship.
 */
struct ShipControl {
	explicit ShipControl(sbg::InputTracker& inputs) noexcept;
	
	void handle(const sbg::KeyboardEvent& event);
	void control(sbg::Entity ship);
	
private:
	sbg::Entity _ship;
	sbg::InputTracker& _inputs;
	sbg::StateMachine _states = Idle{};
};

} // namespace spaceshooter::spaceshooter
