#include "shootingengine.h"

#include "../component/ship.h"

#include "subgine/entity.h"
#include "subgine/system.h"
#include "subgine/log.h"
#include "subgine/system/deferredscheduler.h"
#include "subgine/graphic/component/transform.h"

#include <chrono>
#include <random>

using namespace testgame::spaceshooter;
using namespace std::literals;

ShootingEngine::ShootingEngine(sbg::EntityFactory entityFactory) noexcept : _entityFactory{std::move(entityFactory)} {}

void ShootingEngine::remove(sbg::Entity entity) {
	auto const entity_ship = sbg::Component<Ship>{entity};
	_entities.erase(
		std::remove_if(std::begin(_entities), std::end(_entities),
		[&](auto const components) {
			auto [ship, physic] = components;
			return ship == entity_ship;
		}
	), _entities.end());
}

void ShootingEngine::add(sbg::Entity entity) {
	sbg::Log::fatal([&]{ return !entity.has<Ship>(); }, SBG_LOG_INFO, "The entity don't have a ship component");
	sbg::Log::fatal([&]{ return !entity.has<sbg::PhysicPoint2D>(); }, SBG_LOG_INFO, "The entity don't have a physic component");
	
	_entities.emplace(entity);
}

void ShootingEngine::execute(sbg::Time time) {
	auto now = time.now;
	
	_entities.cleanup();
	
	for (auto const [ship, physic] : _entities) {
		if (ship->fires()) {
			auto tryShoot = [&](Gun& gun) {
				while (gun.lastShot < now - gun.cooldown && now > gun.startsShooting) {
					auto since_shot = (now - gun.lastShot) - gun.cooldown;
					shootWeapon(*ship, physic->getNextPosition(
						std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(-since_shot).count()),
						gun,
						std::chrono::duration_cast<std::chrono::microseconds>(since_shot)
					);
					gun.lastShot += gun.cooldown;
				}
			};
			
			tryShoot(ship->mainWeapon);
			
			if (ship->secondaryWeapon) {
				tryShoot(*ship->secondaryWeapon);
			}
			
			for (auto& gun : ship->otherWeapons) {
				tryShoot(gun);
			}
		}
	}
}

void ShootingEngine::shootWeapon(const Ship& ship, sbg::Vector2d position, const Gun& gun, std::chrono::microseconds since_shot) {
	auto shoot = [&](sbg::Vector2f offset) {
		auto const entity = _entityFactory.create(gun.bullets);
		auto const physic = sbg::Component<sbg::PhysicPoint2D>{entity};
		auto const transform = sbg::Component<sbg::Transform>{entity};
		physic->setPosition(position + offset);
		
		static std::mt19937 random{std::random_device{}()};
		double displacement = std::normal_distribution<float>{0, sbg::tau_f / 16}(random) * (1 - gun.accuracy);
		
		physic->setVelocity(physic->getVelocity().angle(physic->getVelocity().angle() + displacement));
		physic->update(sbg::Time{std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(since_shot).count()});
		
		*transform = transform->position2d(physic->getPosition());
		
		_deferredEngine->defer(1000ms, [entity] {
			entity.destroy();
		});
	};
	
	shoot(gun.offset);
	
	if (gun.mirroired) {
		shoot(gun.offset * sbg::Vector2f{-1, 1});
	}
}

void ShootingEngine::setupMainEngine(sbg::MainEngine& mainEngine, sbg::DeferredScheduler& deferredEngine) {
	_engineOwner = mainEngine.add(*this);
	_deferredEngine = &deferredEngine;
}
