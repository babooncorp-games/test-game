#pragma once

#include "subgine/entity/entity.h"
#include "subgine/entity/componentlist.h"
#include "subgine/entity/creator/entitycreator.h"
#include "subgine/physic/physicpoint.h"
#include "subgine/system/time.h"
#include "subgine/common/ownershiptoken.h"

#include <vector>
#include <chrono>

namespace sbg {
	struct MainEngine;
	struct DeferredScheduler;
}

namespace testgame::spaceshooter {

struct Ship;
struct Gun;

/**
 * This class is a engine that activate weapons of ships.
 * It will create bullets form the shooting ships.
 */
struct ShootingEngine {
	explicit ShootingEngine(sbg::EntityFactory entityFactory) noexcept;
	
	void execute(sbg::Time time);
	void add(sbg::Entity entity);
	void remove(sbg::Entity entity);
	void setupMainEngine(sbg::MainEngine& mainEngine, sbg::DeferredScheduler& deferredEngine);
	
private:
	void shootWeapon(const Ship& ship, sbg::Vector2d position, const Gun& gun, std::chrono::microseconds since_shot);
	
	sbg::DeferredScheduler* _deferredEngine;
	sbg::EntityFactory _entityFactory;
	sbg::OwnershipToken _engineOwner;
	sbg::ComponentList<Ship, sbg::PhysicPoint2D> _entities;
};

} // namespace testgame::spaceshooter
