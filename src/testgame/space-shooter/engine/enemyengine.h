#pragma once

#include "subgine/entity/entity.h"
#include "subgine/entity/component.h"
#include "subgine/entity/entitymanager.h"
#include "subgine/common/ownershiptoken.h"
#include "subgine/system/time.h"
#include "subgine/entity/creator/entitycreator.h"

#include <vector>
#include <memory>
#include <chrono>

namespace sbg {
	struct MainEngine;
}

namespace testgame::spaceshooter {

struct EnemySpawner;

/**
 * This engine will spawn enmies at the right time during the gameplay of a level.
 */
struct EnemyEngine {
	explicit EnemyEngine(sbg::EntityFactory entityFactory, sbg::EntityManager& entityManager) noexcept;
	
	void add(sbg::Entity entity);
	void setupMainEngine(sbg::MainEngine& mainEngine);
	void execute(sbg::Time time);
	
private:
	struct Entry {
		sbg::Component<EnemySpawner> data;
		std::chrono::microseconds spawn_at;
		double offset = 0;
	};
	
	std::chrono::microseconds _progression;
	sbg::EntityManager& _entityManager;
	sbg::EntityFactory _entityFactory;
	sbg::OwnershipToken _engineOwner;
	std::vector<Entry> _entities;
};

} // namespace testgame::spaceshooter
