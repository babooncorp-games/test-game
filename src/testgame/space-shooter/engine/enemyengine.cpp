#include "enemyengine.h"

#include "../component/enemy.h"
#include "../component/enemyspawner.h"

#include "subgine/system.h"
#include "subgine/entity.h"
#include "subgine/physic.h"
#include "subgine/graphic.h"
#include "subgine/log.h"
#include "subgine/tiled.h"

using namespace testgame::spaceshooter;

EnemyEngine::EnemyEngine(sbg::EntityFactory entityFactory, sbg::EntityManager& entityManager) noexcept : _entityManager{entityManager}, _entityFactory{entityFactory} {}

void EnemyEngine::add(sbg::Entity entity) {
	auto spawner = sbg::Component<EnemySpawner>{entity};
	sbg::Log::fatal([&]{ return not spawner; }, SBG_LOG_INFO, "The entity doesn't have a ship component");
	
	auto tiled = sbg::Component<sbg::tiled::Data>{entity};
	auto position = tiled->object->position;
	auto spawn_at = std::chrono::microseconds{static_cast<std::int64_t>(std::micro::den * position.y / std::micro::num)};
	
	sbg::Log::debug(SBG_LOG_INFO, "Adding enemy spawner for", spawner->type, "created at", std::micro::num * static_cast<double>(spawn_at.count()) / std::micro::den);
	
	_entities.emplace_back(Entry{
		sbg::Component<EnemySpawner>{entity},
		spawn_at,
		position.x
	});
	
	std::sort(_entities.begin(), _entities.end(), [](auto const& first, auto const& second) { return first.spawn_at > second.spawn_at; });
}

void EnemyEngine::execute(sbg::Time time) {
	_progression += std::chrono::microseconds{static_cast<std::int64_t>(std::micro::den * time.current / std::micro::num)};
	
	if (!_entities.empty()) {
		auto next = _entities.back();
		
		if (next.spawn_at <= _progression) {
			_entities.erase(_entities.end() - 1);
			
			auto l = sbg::Log::debug(SBG_LOG_INFO, "Spawning enemy", next.data->type, "at", std::micro::num * static_cast<double>(_progression.count()) / std::micro::den);
			auto entity = _entityManager.create();
			
			entity.assign(*next.data);
			_entityFactory.create(next.data->type, entity);
			
			auto const enemy = sbg::Component<Enemy>{entity};
			auto const transform = sbg::Component<sbg::Transform>{entity};
			enemy->offset = next.offset;
			
			*transform = transform->position2d(sbg::Vector2d{enemy->offset - (1920 / 2), -40000});
		}
	}
}

void EnemyEngine::setupMainEngine(sbg::MainEngine& mainEngine) {
	_engineOwner = mainEngine.add(*this);
}
