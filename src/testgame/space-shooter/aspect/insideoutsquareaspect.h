#pragma once

#include "subgine/shape/alignedbox.h"

namespace testgame::spaceshooter {

struct InsideoutSquareAspect : sbg::shape::AlignedBox2D {
	explicit InsideoutSquareAspect(sbg::Vector2d top, sbg::Vector2d bottom) noexcept;
};

} // namespace spaceshooter::testgame
