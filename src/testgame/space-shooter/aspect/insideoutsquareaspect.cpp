#include "insideoutsquareaspect.h"

using namespace testgame::spaceshooter;

InsideoutSquareAspect::InsideoutSquareAspect(sbg::Vector2d top, sbg::Vector2d bottom) noexcept : AlignedBox{top, bottom} {}
