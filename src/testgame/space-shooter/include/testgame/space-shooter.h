#pragma once

#include "../../algorithm/alignedboxinsidealgorithm.h"
#include "../../aspect/insideoutsquareaspect.h"

#include "../../component/enemy.h"
#include "../../component/enemyspawner.h"
#include "../../component/health.h"
#include "../../component/ship.h"

#include "../../engine/enemyengine.h"

#include "../../engine/shootingengine.h"

#include "../../state/idle.h"
#include "../../state/moving.h"

#include "../../gun.h"
#include "../../shipcontrol.h"
