#pragma once

#include "subgine/collision/shapeinterface.h"
#include "subgine/collision/manifold.h"
#include "subgine/collision/algorithm/alignedboxalignedboxalgorithm.h"
#include "subgine/common/math.h"
#include "subgine/log.h"

#include <optional>

namespace testgame::spaceshooter {

struct AlignedBoxInsideAlgorithm {
	template<typename T1, typename T2>
	std::optional<sbg::Manifold2D> operator()(const T1& a, const T2& b) const {
		if (sbg::AlignedBoxContainedAlgorithm<2>{}(b, a)) {
			return {};
		}
		
		auto const boxA = aligned_box_shape(a);
		auto const boxB = aligned_box_shape(b);
		sbg::Manifold2D manifold{};
		
		auto extentA = (boxA.bottom - boxA.top) / 2;
		auto extentB = (boxB.bottom - boxB.top) / 2;
		
		auto relative = (boxB.bottom + boxB.top) / 2 - (boxA.bottom + boxA.top) / 2;
		
		sbg::Vector2d overlap{
			(extentA.x + extentB.x - std::abs(relative.x)),
			(extentA.y + extentB.y - std::abs(relative.y))
		};
		
		if (overlap.x > 0 && overlap.y > 0) {
			auto minimum = std::min({overlap.x, overlap.y});
			
			manifold.contacts[0].penetration = sbg::Vector2d{
				(overlap.x == minimum ? (overlap.x - (boxA.bottom - boxA.top).x) * (relative.x >= 0 ? 1.:-1.) : 0),
				(overlap.y == minimum ? (overlap.y - (boxA.bottom - boxA.top).y) * (relative.y >= 0 ? 1.:-1.) : 0)
			};
		}
		
		return manifold;
	}
};

} // namespace testgame::spaceshooter
