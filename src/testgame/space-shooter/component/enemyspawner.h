#pragma once

#include <string>

namespace testgame::spaceshooter {

struct EnemySpawner {
	std::string type;
	std::string path;
	std::string loop;
	std::string shooting;
	double duration;
};

} // namespace testgame::spaceshooter
