#include "ship.h"

#include <chrono>

using namespace testgame::spaceshooter;

bool Ship::fires() const noexcept {
	return _fires;
}

void Ship::startFire() noexcept {
	auto setupWeapon = [](Gun& gun) {
		gun.startsShooting = std::chrono::high_resolution_clock::now() + gun.initialDelay;
		gun.lastShot = gun.startsShooting + gun.cooldown;
	};
	
	setupWeapon(mainWeapon);
	
	if (secondaryWeapon) {
		setupWeapon(*secondaryWeapon);
	}
	
	for (auto& gun : otherWeapons) {
		setupWeapon(gun);
	}
	
	_fires = true;
}

void Ship::stopFire() noexcept {
	_fires = false;
}

