#pragma once

#include "subgine/animation/engine/animationengine.h"

#include <optional>

namespace testgame::spaceshooter {

struct Enemy {
	double offset;
	sbg::AnimationEngine::AnimationHandle path;
	sbg::AnimationEngine::AnimationHandle loop;
	sbg::AnimationEngine::AnimationHandle firering;
	//std::optional<sbg::Animation<sbg::Vector2d, LinearTransition>> loop;
	//sbg::Animation<bool, sbg::NearestTransition> firering;
};

} // namespace testgame::spaceshooter

