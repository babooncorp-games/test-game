#pragma once

#include <string>
#include <chrono>

namespace testgame::spaceshooter {

struct Health {
	// TODO: Remove in visual studio in next update
	Health(int _health, int _shield, int _max_health, int _max_shield, int _max_damage, std::string _death, std::chrono::milliseconds _delay) noexcept :
		health{_health},
		shield{_shield},
		max_health{_max_health},
		max_shield{_max_shield},
		max_damage{_max_damage},
		death{std::move(_death)},
		delay{_delay} {}

	int health;
	int shield;
	int max_health;
	int max_shield;
	int max_damage;
	std::string death;
	std::chrono::milliseconds delay;
	bool dead = false;
	int last_health = health;
	int last_shield = shield;
	
	void save_last();
};

} // namespace testgame::spaceshooter
