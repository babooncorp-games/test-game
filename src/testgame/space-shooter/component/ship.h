#pragma once

#include "../gun.h"

#include <optional>
#include <vector>

namespace testgame::spaceshooter {

/**
 * Minimal data for any ship kind
 */
struct Ship {
	Gun mainWeapon;
	std::optional<Gun> secondaryWeapon;
	std::vector<Gun> otherWeapons;
	
	bool fires() const noexcept;
	
	void startFire() noexcept;
	void stopFire() noexcept;
	
private:
	bool _fires = false;
};

} // namespace testgame::spaceshooter
