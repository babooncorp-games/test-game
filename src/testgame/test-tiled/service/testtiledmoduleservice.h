#pragma once

#include "../testtiledmodule.h"

#include "subgine/scene/service/scenecreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace testgame::testtiled {

struct TestTiledModuleService : kgr::single_service<TestTiledModule>,
	sbg::autocall<
		&TestTiledModule::setupSceneCreator,
		&TestTiledModule::setupEntityBindingCreator
	> {};

auto service_map(TestTiledModule const&) -> TestTiledModuleService;

} // namespace testgame::testtiled
