#include "testtiledmodule.h"

#include "subgine/animation.h"
#include "subgine/animation/service.h"
#include "subgine/resource.h"
#include "subgine/scene.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/graphic/service.h"
#include "subgine/provider.h"
#include "subgine/provider/service.h"
#include "subgine/system.h"
#include "subgine/system/service.h"
#include "subgine/tiled.h"
#include "subgine/tiled/service.h"

using namespace testgame::testtiled;
using namespace std::literals;

void TestTiledModule::setupSceneCreator(sbg::SceneCreator& sceneCreator) const {
	sceneCreator.add("test-tiled-map", [](sbg::Camera& camera, sbg::ProviderFactory<glm::mat4> providerFactory, sbg::tiled::MapFactory tiledMapFactory, sbg::tiled::MapMaterializer materializer, sbg::Property data) {
		return [&camera, providerFactory, tiledMapFactory, materializer, name = std::string{data["name"]}, proj = data["default-camera-projection"]] {
			camera.projection = providerFactory.create(proj["type"], sbg::Entity{}, proj["data"]);
			
			auto map = tiledMapFactory.create(name);
			auto materializedMap = materializer.materialize(map);
		};
	});
}

void TestTiledModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("animate-towards-lover", [](sbg::DeferredScheduler& scheduler, sbg::AnimationEngine& animations, sbg::Entity entity) {
		scheduler.defer(500ms, [&animations, entity] {
			if (not entity) return;
			auto const tiledData = sbg::Component<sbg::tiled::Data>{entity};
			
			if (auto const lover = tiledData->getOptionalLinkedEntity(*tiledData->object, "Lover")) {
				auto const transform = sbg::Component<sbg::Transform>{entity};
				
				auto const positionStart = transform->position2d();
				auto const positionEnd = lover.component<sbg::Transform>().position2d() - sbg::Vector2f{18, 0};
				
				auto animation = animations.add(
					std::vector{sbg::Keyframe{positionStart, 1.5}, sbg::Keyframe{positionEnd, 0}},
					sbg::Sine{}, sbg::Clamp{},
					[transform](sbg::Vector2f position) {
						*transform = transform->position2d(position);
					}
				);
				
				animation.attach(entity);
				animation.start();
			}
		});
	});
}
