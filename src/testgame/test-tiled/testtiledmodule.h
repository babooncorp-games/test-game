#pragma once

namespace sbg {
	struct SceneCreator;
	struct EntityBindingCreator;
} // namespace sbg

namespace testgame::testtiled {

struct TestTiledModule {
	void setupSceneCreator(sbg::SceneCreator& sceneCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
};

} // namespace testgame::testtiled
