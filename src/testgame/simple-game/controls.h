#pragma once

#include "subgine/window/inputtracker.h"
#include "subgine/entity/entity.h"
#include "subgine/window/event/keyboardevent.h"

namespace testgame::simplegame {

struct Controls {
	Controls(sbg::InputTracker& tracker);
	
	void control(sbg::Entity entity);
	void handle(const sbg::KeyboardEvent& event);
	
private:
	sbg::Entity _target;
	sbg::InputTracker& _tracker;
};

} // namespace testgame::simplegame
