#include "controls.h"

#include "action/jumpaction.h"
#include "action/summonprojectile.h"

#include "subgine/actor.h"
#include "subgine/entity.h"
#include "subgine/window.h"
#include "subgine/actor-physic.h"
#include "subgine/log.h"

using namespace testgame::simplegame;

Controls::Controls(sbg::InputTracker& tracker) : _tracker{tracker} {
}

void Controls::control(sbg::Entity entity) {
	sbg::Log::trace(SBG_LOG_INFO, "Entity set to controls, instance", this);
	_target = entity;
}

void Controls::handle(const sbg::KeyboardEvent& event) {
	if (_target) {
		auto actor = sbg::Component<sbg::Actor>{_target};
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::Left ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Right) {
			auto movement = sbg::Vector2d{};
			
			movement.x -= _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Left);
			movement.x += _tracker.isKeyDown(sbg::KeyboardEvent::KeyCode::Right);
			
			actor->act(sbg::Move2DAction{movement});
		}
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::X && event.pressed) {
			actor->act(SummonProjectile{});
		}
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::Z && event.pressed) {
			actor->act(JumpAction{});
		}
	}
}
