#include "simplegamemodule.h"

#include "testgame/simple-game.h"

#include "service/controlsservice.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/actor.h"
#include "subgine/scene.h"
#include "subgine/state.h"
#include "subgine/resource.h"

#include "subgine/system/service.h"
#include "subgine/entity/service.h"
#include "subgine/actor/service.h"

using namespace testgame::simplegame;

void SimpleGameModule::setupActorCreator(sbg::ActorCreator& actorCreator) const {
	actorCreator.add("hero-state", [](sbg::Entity entity) {
		return HeroStateHandler{entity};
	});
	
	actorCreator.add("summon-projectile", [](sbg::EntityFactory ef, sbg::Entity entity) {
		return SummonProjectileHandler{ef, entity};
	});
	
	actorCreator.add("jump", [](sbg::Entity entity) {
		return JumpHandler{entity};
	});
}

void SimpleGameModule::setupComponentCreator(sbg::ComponentCreator& componentCreator) const {
	componentCreator.add("hero-statemachine", [](sbg::Entity entity) {
		return sbg::StateMachine{Idle{{1, 0}}};
	});
	
	componentCreator.add("hero", [](sbg::Entity entity) {
		return Hero{1};
	});
}

void SimpleGameModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const {
	collisionReactorCreator.add("reset-jumps", [](sbg::Entity entity, sbg::Property data) {
		auto const hero = sbg::Component<Hero>{entity};
		return [hero](sbg::BasicCollisionInfo info) {
			if (hero) {
				hero->resetJumps();
			}
		};
	});
	
	collisionReactorCreator.add("smash", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity, sbg::Property data) {
		return [entity, &scheduler](sbg::BasicCollisionInfo info) {
			scheduler.defer([entity]{ entity.destroy(); });
		};
	});
}

void SimpleGameModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("2dsidescroll-controls", [](Controls& controls, sbg::Entity entity) {
		controls.control(entity);
	});
}

void SimpleGameModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("simplegame-controls", [](sbg::EventDispatcher& events, Controls& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}
