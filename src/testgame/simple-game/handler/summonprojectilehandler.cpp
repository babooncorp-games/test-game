#include "summonprojectilehandler.h"

#include "../state/herostate.h"

#include "subgine/aseprite.h"
#include "subgine/entity.h"
#include "subgine/physic.h"
#include "subgine/state.h"

using namespace testgame::simplegame;

SummonProjectileHandler::SummonProjectileHandler(sbg::EntityFactory entityFactory, sbg::Entity entity) : _entityFactory{entityFactory}, _entity{entity} {}

void SummonProjectileHandler::handle(SummonProjectile action) {
	auto const state = sbg::Component<sbg::StateMachine>{_entity};
	auto const physicPoint = sbg::Component<sbg::PhysicPoint2D>{_entity};
	
	auto projectile = _entityFactory.create("projectile");
	auto const projectilePhysicPoint = sbg::Component<sbg::PhysicPoint2D>(projectile);
	
	auto entityPosition = physicPoint->getPosition();
	sbg::Vector2d velocity;
	
	auto spawn = [&](sbg::Vector2d direction) {
		entityPosition.x += direction.x * 16.0;
		velocity.x = 150 * direction.x;
		
		auto const asepriteAnimation = sbg::Component<sbg::AsepriteAnimation>{projectile};
		if (direction.x > 0) { // Right
			asepriteAnimation->select("flying_right");
		} else if (direction.x < 0) { // Left
			asepriteAnimation->select("flying_left");
		}
	};
	
	state->transit(
		[&](Idle idle) { spawn(idle.direction); },
		[&](Run run) { spawn(run.direction); }
	);
	
	projectilePhysicPoint->setPosition(std::move(entityPosition));
	projectilePhysicPoint->setVelocity(std::move(velocity));
}
