#include "herostatehandler.h"

#include "../state/herostate.h"
#include "../state/herotransition.h"

#include "subgine/entity.h"
#include "subgine/state.h"

using namespace testgame::simplegame;

HeroStateHandler::HeroStateHandler(sbg::Entity entity) : _entity{entity} {}

void HeroStateHandler::handle(sbg::Move2DAction action) {
	auto const state = sbg::Component<sbg::StateMachine>{_entity};
	
	state->transit(HeroTransition{action.movement, _entity});
}
