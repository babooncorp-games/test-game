#pragma once

#include "subgine/entity/entity.h"
#include "subgine/actor-physic/moveaction.h"

namespace testgame::simplegame {

struct HeroStateHandler {
	HeroStateHandler(sbg::Entity entity);
	void handle(sbg::Move2DAction action);
	
private:
	sbg::Entity _entity;
};

} // namespace testgame::simplegame
