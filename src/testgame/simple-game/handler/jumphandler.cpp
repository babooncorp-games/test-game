#include "jumphandler.h"

#include "../component/hero.h"

#include "subgine/entity.h"
#include "subgine/physic.h"

using namespace testgame::simplegame;

JumpHandler::JumpHandler(sbg::Entity entity) : _entity{entity}  {}

void JumpHandler::handle(JumpAction action) {
	if (_entity) {
		auto const physicPoint = sbg::Component<sbg::PhysicPoint2D>{_entity};
		auto const hero = sbg::Component<Hero>{_entity};
		
		if (hero->getRemainingJumps() > 0) {
			hero->jump();
			physicPoint->accumulatePulse("jump", {0, -3000});
		}
	}
}
