#pragma once

#include "../action/summonprojectile.h"

#include "subgine/entity/entity.h"
#include "subgine/entity/creator/entitycreator.h"

namespace testgame::simplegame {

struct SummonProjectileHandler {
	SummonProjectileHandler(sbg::EntityFactory entityFactory, sbg::Entity entity);
	void handle(SummonProjectile action);
	
private:
	sbg::EntityFactory _entityFactory;
	sbg::Entity _entity;
};

} // namespace testgame::simplegame
