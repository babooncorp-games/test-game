#pragma once

#include "../action/jumpaction.h"

#include "subgine/entity/entity.h"

namespace testgame::simplegame {

struct JumpHandler {
	JumpHandler(sbg::Entity entity);
	void handle(JumpAction action);
	
private:
	sbg::Entity _entity;
};

} // namespace testgame::simplegame
