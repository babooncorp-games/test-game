#pragma once

namespace testgame::simplegame {

struct Hero {
	inline Hero(int maxJumps) : _maxJumps{maxJumps}, _remainingJumps{maxJumps} {}
	inline void resetJumps() { _remainingJumps = _maxJumps; }
	inline void setMaxJumps(int maxJumps) { _maxJumps = maxJumps; }
	inline void jump() { if (_remainingJumps > 0) { _remainingJumps--; } }
	inline auto getRemainingJumps() -> int { return _remainingJumps; }
	
private:
	int _maxJumps;
	int _remainingJumps;
};

} // namespace testgame::simplegame
