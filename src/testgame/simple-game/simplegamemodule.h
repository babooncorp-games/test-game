#pragma once

namespace sbg {
	struct ActorCreator;
	struct ComponentCreator;
	struct CollisionReactorCreator;
	struct EntityBindingCreator;
	struct PluggerCreator;
}

namespace testgame::simplegame {

struct SimpleGameModule {
	void setupActorCreator(sbg::ActorCreator& actorCreator) const;
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const;
	void setupComponentCreator(sbg::ComponentCreator& componentCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
};

} // namespace testgame::simplegame
