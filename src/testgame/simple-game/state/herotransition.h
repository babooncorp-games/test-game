#pragma once

#include "herostate.h"

#include "subgine/entity/entity.h"
#include "subgine/state/utility/from.h"

#include <optional>

namespace testgame::simplegame {

struct HeroTransition : sbg::From<Idle, Run> {
	inline explicit HeroTransition(sbg::Vector2d movement, sbg::Entity entity) : _movement{movement}, _entity{entity} {}
	std::optional<Run> transit(Idle idle);
	std::optional<Idle> transit(Run run);
	
private:
	sbg::Vector2d _movement;
	sbg::Entity _entity;
};

} // namespace testgame::simplegame
