#include "herotransition.h"

#include "../state/herostate.h"

#include "subgine/aseprite.h"
#include "subgine/graphic.h"
#include "subgine/state.h"

using namespace testgame::simplegame;

std::optional<Run> HeroTransition::transit(Idle idle) {
	auto& asepriteAnimation = _entity.component<sbg::AsepriteAnimation>();
	if (_movement.x > 0) { // Right
		asepriteAnimation.select("run_right");
	} else if (_movement.x < 0) { // Left
		asepriteAnimation.select("run_left");
	}
	
	if (_movement.x == 0) {
		return {};
	} else {
		return Run{_movement.unit()};
	}
}

std::optional<Idle> HeroTransition::transit(Run run) {
	if (_movement.x == 0) {
		auto& asepriteAnimation = _entity.component<sbg::AsepriteAnimation>();
		if (run.direction.x > 0) { // Right
			asepriteAnimation.select("idle_right");
		} else if (run.direction.x < 0) { // Left
			asepriteAnimation.select("idle_left");
		}
		return Idle{run.direction.unit()};
	}
	return {};
}
