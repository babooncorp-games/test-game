#pragma once

#include "subgine/vector/vector.h"

namespace testgame::simplegame {

struct Idle {
	sbg::Vector2d direction;
};

struct Run {
	sbg::Vector2d direction;
};

} // namespace testgame::simplegame
