#pragma once

#include "subgine/common/kangaru.h"

#include "../simplegamemodule.h"

namespace testgame::simplegame {

struct SimpleGameModuleService : kgr::single_service<SimpleGameModule>,
	sbg::autocall<
		&SimpleGameModule::setupActorCreator,
		&SimpleGameModule::setupCollisionReactorCreator,
		&SimpleGameModule::setupComponentCreator,
		&SimpleGameModule::setupEntityBindingCreator,
		&SimpleGameModule::setupPluggerCreator
	> {};

auto service_map(const SimpleGameModule&) -> SimpleGameModuleService;

} // namespace testgame::simplegame

