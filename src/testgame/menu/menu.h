#pragma once

#include "subgine/entity/entity.h"
#include "subgine/entity/component.h"
#include "subgine/graphic/component/transform.h"
#include "subgine/scene/scenemanager.h"
#include "subgine/common/ownershiptoken.h"

#include <vector>
#include <tuple>
#include <memory>

namespace sbg {
	struct View;
	struct MainEngine;
}

namespace testgame {

struct Button;
struct ButtonSprite;

/**
 * Will hold all the buttons and manage button selection, action triggering and scene switching
 */
struct Menu {
	explicit Menu(sbg::SceneManager& sceneFactory, sbg::Scene& menuScene, sbg::View& view) noexcept;
	void addButton(sbg::Entity entity);
	
	void press() noexcept;
	void release() noexcept;
	
	void next() noexcept;
	void previous() noexcept;
	
	// TODO: Move animation into the drawing process
	void setupMainEngine(sbg::MainEngine& mainEngine);
private:
	
	struct Entry {
		sbg::Component<Button> button;
		sbg::Component<sbg::Transform> transform;
		ButtonSprite* sprite;
	};
	
	void select(std::vector<Entry>::iterator selected);
	int nthselected() const noexcept;
	
	float _offset = 0;
	float _progression = 1;
	float _direction = 0;
	float _bias = 0;
	std::vector<Entry> _buttons;
	std::vector<Entry>::iterator _selected = _buttons.end();
	sbg::SceneManager& _sceneManager;
	sbg::Scene& _menuScene;
	sbg::View& _view;
	sbg::OwnershipToken _menuAnimationOwner;
};

} // namespace testgame
