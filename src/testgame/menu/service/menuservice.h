#pragma once

#include "../menu.h"

#include "subgine/scene/service/currentsceneservice.h"
#include "subgine/scene/service/scenemanagerservice.h"
#include "subgine/graphic/service/viewservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace testgame {

struct MenuService : kgr::single_service<Menu,
	kgr::dependency<
		sbg::SceneManagerService,
		sbg::CurrentSceneService,
		sbg::ViewService
	>
>, sbg::autocall<&Menu::setupMainEngine> {};

auto service_map(const Menu&) -> MenuService;

} // namespace testgame
