#pragma once

#include "subgine/common/kangaru.h"
#include "subgine/scene/scene.h"

namespace testgame {

struct PreviousSceneService : kgr::extern_service<sbg::Scene> {};

} // namespace testgame
