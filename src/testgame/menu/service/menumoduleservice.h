#pragma once

#include "../menumodule.h"

#include "subgine/scene/service/scenecreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/provider/service/providercreatorservice.h"
#include "subgine/graphic/service/environmentuniformcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace testgame {

struct MenuModuleService : kgr::single_service<MenuModule>,
	sbg::autocall<
		&MenuModule::setupSceneCreator,
		&MenuModule::setupModelCreator,
		&MenuModule::setupPluggerCreator,
		&MenuModule::setupProviderCreator,
		&MenuModule::setupEntityBindingCreator,
		&MenuModule::setupEnvironmentUniformCreator
	> {};

auto service_map(const MenuModule&) -> MenuModuleService;

} // namespace testgame
