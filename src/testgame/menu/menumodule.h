#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

namespace sbg {
	struct ModelCreator;
	struct EntityBindingCreator;
	struct SceneCreator;
	struct PluggerCreator;
	template<typename> struct ProviderCreator;
	struct EnvironmentUniformCreator;
}

namespace testgame {

struct MenuModule {
	void setupSceneCreator(sbg::SceneCreator& sceneCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& sceneCreator) const;
	void setupModelCreator(sbg::ModelCreator& modelCreator) const;
	void setupProviderCreator(sbg::ProviderCreator<glm::mat4>& providerCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupEnvironmentUniformCreator(sbg::EnvironmentUniformCreator& environmentUniformCreator) const;
};

} // namespace testgame
