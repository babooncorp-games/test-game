#pragma once

#include <string>

#include "subgine/vector/vector.h"

namespace testgame {

/**
 * Contains everything needed to make a button.
 * It is assigned to the entity before the factory is called.
 */
struct Button {
	std::string scene;
	std::u8string text;
};

} // namespace testgame
