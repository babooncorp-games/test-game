#version 140

out vec4 outColor;
in vec2 UV;

uniform int state;
uniform sampler2DArray background;

void main() {
	vec4 back_color = texture(background, vec3(UV, state));
	
	outColor = vec4(back_color.rgb, 1 - pow( 1 - back_color.a, 1.0 / 2.2));
}
