#version 140

in vec3 vertexPosition;
in vec2 texturePosition;

out vec2 UV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
	gl_Position = projection * view * model * vec4(vertexPosition,1);
	UV = texturePosition;
}
