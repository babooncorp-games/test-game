#pragma once

#include "subgine/vector/vector.h"
#include "subgine/entity/component.h"
#include "subgine/graphic/component/transform.h"
#include "subgine/graphic/utility/uniformencoderforward.h"

namespace testgame {

/**
 * A sprite made to also hold the button state
 */
struct ButtonSprite {
	sbg::Component<sbg::Transform> transform;
	sbg::Transform origin;
	bool visible = true;
	
	enum struct State {
		Normal,
		Focus,
		Pressed,
		Disabled
	} state{};
	
	static void serialize(sbg::ModelUniformEncoder& uniforms, ButtonSprite const& sprite);
};

} // namespace testgame
