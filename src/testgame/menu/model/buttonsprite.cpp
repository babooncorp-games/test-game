#include "buttonsprite.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/utility/transformmodelmatrix.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace testgame;
using namespace sbg::literals;

void ButtonSprite::serialize(sbg::ModelUniformEncoder& uniforms, ButtonSprite const& sprite) {
	uniforms
		<< sbg::uniform::updating("state"_h, [](ButtonSprite const& sprite) noexcept {
			return static_cast<int>(sprite.state);
		})
		<< sbg::uniform::updating("model"_h, sbg::TransformModelMatrix<ButtonSprite>{})
		<< sbg::uniform::value("visible"_h, sprite.visible);
}
