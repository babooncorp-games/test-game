#include "menu.h"

#include "component/button.h"
#include "model/buttonsprite.h"

#include "service/previoussceneservice.h"

#include "subgine/scene/service.h"
#include "subgine/system.h"
#include "subgine/text.h"
#include "subgine/log.h"
#include "subgine/graphic.h"

#include <cmath>

using namespace testgame;

static void apply_position(sbg::Transform& transform, float offset, int nth) {
	transform = transform.position2d(sbg::Vector2f{0, (nth + offset) * 150.f});
}

Menu::Menu(sbg::SceneManager& sceneManager, sbg::Scene& menuScene, sbg::View& view) noexcept :
	_sceneManager{sceneManager}, _menuScene{menuScene}, _view{view} {}

void Menu::addButton(sbg::Entity entity) {
	if (entity.has<Button>() && entity.has<sbg::Visual>()) {
		auto& visual = entity.component<sbg::Visual>();
		auto const button = sbg::Component<Button>{entity};
		auto const transform = sbg::Component<sbg::Transform>{entity};
		
		sbg::Log::error(
			[&]{ return !visual.has<ButtonSprite>("button"); },
			SBG_LOG_INFO, "The added button does not have a testgame::ButtonSprite model named button"
		);
	
		if (_selected != _buttons.end()) {
			_selected->sprite->state = ButtonSprite::State::Normal;
		}
		
		apply_position(*transform, _offset, _buttons.size());
		
		_buttons.emplace_back(button, transform, &visual.inspect<ButtonSprite>("button"));
		_view.reserialize("button", entity);
		_selected = _buttons.end();
	} else {
		sbg::Log::warning(SBG_LOG_INFO, "The added entity don't have the button or visual component");
	}
}

void Menu::press() noexcept {
	if (_selected != _buttons.end()) {
		_selected->sprite->state = ButtonSprite::State::Pressed;
	}
}

void Menu::release() noexcept {
	if (_selected != _buttons.end()) {
		if (_selected->sprite->state == ButtonSprite::State::Pressed) {
			
			auto& scene = _sceneManager.create(_selected->button->scene);
			
			// We take the parent here, cause its the parent that has the whole theme.
			// Probably could be improved.
			_menuScene.parent([&scene](sbg::Scene& parentScene){
				scene.container().emplace<PreviousSceneService>(parentScene);
			});
			
			_sceneManager.plug(scene);
			_sceneManager.load(scene);
		}
		
		_selected->sprite->state = ButtonSprite::State::Focus;
	}
}

void Menu::next() noexcept {
	if (_selected != _buttons.end()) {
		auto it = std::find_if(_selected + 1, _buttons.end(), [](const auto& button) {
			return button.sprite->state != ButtonSprite::State::Disabled;
		});
		
		if (it != _buttons.end()) {
			_progression = 0;
			_direction = 1;
			_bias = 1;
			select(it);
		}
	} else {
		select(_buttons.begin());
	}
}

void Menu::previous() noexcept {
	if (_selected != _buttons.end()) {
		auto it = std::find_if(
			_buttons.rbegin() + std::distance(_selected, _buttons.end()),
			_buttons.rend(),
			[](const auto& button) {
				return button.sprite->state != ButtonSprite::State::Disabled;
			}
		);
		
		if (it != _buttons.rend()) {
			_progression = 0;
			_direction = -1;
			_bias = -1;
			select(_buttons.end() - std::distance(_buttons.rbegin(), it + 1));
		}
	} else {
		select(_buttons.begin());
	}
}

void Menu::setupMainEngine(sbg::MainEngine& mainEngine) {
	_menuAnimationOwner = mainEngine.add([&](sbg::Time time) {
		if (_progression < 1.f) {
			_progression += time.current / 0.2f;
			_progression = std::min(_progression, 1.f);
			_offset = -1 * (nthselected() + _direction * std::sin(((_progression) * (sbg::tau_f/4.f)))) + _bias;
			
			// TODO: C++20 use zip range
			auto position = int{};
			for (auto const& [button, transform, sprite] : _buttons) {
				apply_position(*transform, _offset, position);
				++position;
			}
		}
	});
}

int Menu::nthselected() const noexcept {
	if (_selected != _buttons.end()) {
		return std::distance(_buttons.begin(), std::vector<Entry>::const_iterator{_selected});
	} else {
		return 0;
	}
}

void Menu::select(std::vector<Entry>::iterator selected) {
	if (_selected != _buttons.end()) {
		_selected->sprite->state = ButtonSprite::State::Normal;
	}
	_selected = selected;
	_selected->sprite->state = ButtonSprite::State::Focus;
}

