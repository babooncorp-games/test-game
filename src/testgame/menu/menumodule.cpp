#include "menumodule.h"

#include "testgame/menu.h"

#include "testgame/menu/service.h"

#include "subgine/text.h"
#include "subgine/scene.h"
#include "subgine/graphic.h"
#include "subgine/provider.h"
#include "subgine/opengl.h"
#include "subgine/resource.h"
#include "subgine/window.h"
#include "subgine/event.h"
#include "subgine/entity.h"
#include "subgine/log.h"
#include "subgine/system.h"

#include "subgine/text/service.h"
#include "subgine/provider/service.h"
#include "subgine/opengl/service.h"
#include "subgine/event/service.h"
#include "subgine/entity/service.h"
#include "subgine/graphic/service.h"
#include "subgine/window/service.h"
#include "subgine/scene/service.h"
#include "subgine/system/service.h"
#include "subgine/tiled/service.h"

using namespace testgame;
using namespace std::literals;

void MenuModule::setupSceneCreator(sbg::SceneCreator& sceneCreator) const {
	sceneCreator.add("menu", [](sbg::ProviderFactory<glm::mat4> providers, sbg::EntityManager& entities, sbg::EntityFactory entityFactory, sbg::Camera& camera, sbg::Property data) {
		return [&entities, providers, &camera, data, entityFactory] {
			camera.projection = providers.create("window-ortho-transform-fixed-width", sbg::Entity{}, data);
			
			for (auto const button : data["buttons"]) {
				auto entity = entities.create();
				
				entity.assign(Button{std::string{button["scene"]}, std::u8string{button["text"]}});
				entity.assign(sbg::Transform{});
				entityFactory.create("button", entity);
			}
		};
	});
	
	sceneCreator.add("sprite-scene", [](sbg::ProviderFactory<glm::mat4> providers, sbg::EntityFactory entities, sbg::Camera& camera, sbg::Property data) {
		return [&camera, providers, entities, data] {
			camera.projection = providers.create("window-ortho-transform", sbg::Entity{}, sbg::Property{});
			entities.create(data["sprite"]);
		};
	});
	
	sceneCreator.add("text-scene", [](sbg::ProviderFactory<glm::mat4> providers, sbg::MsdfFontManager fonts, kgr::lazy<sbg::ViewService> view, sbg::EntityManager& entities, sbg::MainEngine& me, sbg::Camera& camera, sbg::Property data) {
		return [&camera, providers, fonts, &entities, &me, view, data]() mutable {
			camera.projection = providers.create("window-ortho-transform", sbg::Entity{}, sbg::Property{});
			auto entity = entities.create();
			auto visual = sbg::Visual{};
			auto model = sbg::TextModel{
				.text = u8"A quick brown fox jumps over the lazy dog.",
				.transform = sbg::Component<sbg::Transform>{entity},
				.size = 128,
				.font = sbg::FontTag{"Droid Sans"},
				.msdf = fonts.get("Droid Sans")
			};
			
			model.borderColor = {0, 0.2, 0.1, 1};
			model.borderThickness = 1.5;
			model.align = sbg::TextHorizontalAlign::center;
			
			entity.assign(me.add([visual = sbg::Component<sbg::Visual>{entity}, acc = 0.f](sbg::Time time) mutable {
				visual->visit("text1", [&](sbg::TextModel& text) {
					acc += time.current;
					text.size = std::sin(acc / 2) * 256 + 270;
				});
			}));
			
			entity.assign(sbg::Transform{});
			
			visual.add("text1", std::move(model), "text");
			entity.assign(std::move(visual));
			view->add(entity);
			(void) data;
		};
	});
}

void MenuModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("menu-controls", [](sbg::Window& window, kgr::lazy<MenuService> menu) {
		struct Subscriber {
			kgr::lazy<MenuService> menu;
			sbg::Window& window;
			
			void handle(sbg::KeyboardEvent event) {
				if (!event.pressed) {
					if (event.keyCode == sbg::KeyboardEvent::KeyCode::Enter || event.keyCode == sbg::KeyboardEvent::KeyCode::Space) {
						menu->release();
					}
				} else if (event.keyCode == sbg::KeyboardEvent::KeyCode::Enter || event.keyCode == sbg::KeyboardEvent::KeyCode::Space) {
					menu->press();
				} else {
					if (event.keyCode == sbg::KeyboardEvent::KeyCode::Up || event.keyCode == sbg::KeyboardEvent::KeyCode::Left) {
						menu->previous();
					} else if (event.keyCode == sbg::KeyboardEvent::KeyCode::Down || event.keyCode == sbg::KeyboardEvent::KeyCode::Right) {
						menu->next();
					}
				}
			}
			
			void plug() {
				window.getEventDispatcher().subscribe(*this);
			}
			
			void unplug() {
				window.getEventDispatcher().unsubscribe(*this);
			}
		};
		
		return Subscriber{std::move(menu), window};
	});
	
	pluggerCreator.add("global-controls", [](sbg::Window& window, sbg::SceneManager& sceneManager, sbg::Scene& currentScene, sbg::DeferredScheduler& scheduler) {
		struct Subscriber {
			sbg::SceneManager& sceneManager;
			sbg::Scene& currentScene;
			sbg::Window& window;
			sbg::DeferredScheduler& scheduler;
			
			void handle(sbg::KeyboardEvent event) {
				if (event.keyCode == sbg::KeyboardEvent::KeyCode::Escape) {
					auto hasPrevious = currentScene.container().contains<PreviousSceneService>();
					if (!event.pressed && hasPrevious) {
						auto& previous = currentScene.container().service<PreviousSceneService>();
						sceneManager.plug(previous);
						scheduler.defer([&sceneManager = sceneManager, &currentScene = currentScene]{
							sceneManager.destroy(currentScene);
						});
					} else if (event.pressed && !hasPrevious) {
						window.close();
					}
				}
			}
			
			void plug() {
				window.getEventDispatcher().subscribe(*this);
			}
			
			void unplug() {
				window.getEventDispatcher().unsubscribe(*this);
			}
		};
		
		return Subscriber{sceneManager, currentScene, window, scheduler};
	});
	
	pluggerCreator.add("menu-container", [](sbg::SceneGraph& graph, sbg::Scene& currentScene) {
		return [&] {
			auto hasPrevious = currentScene.container().contains<PreviousSceneService>();
			if (hasPrevious) {
				auto& previous = currentScene.container().service<PreviousSceneService>();
				auto& currentNode = graph.node(currentScene);
				for (auto& childScene : currentNode.children) {
					childScene.scene->container().emplace<PreviousSceneService>(previous);
				}
			}
		};
	});
}

void MenuModule::setupProviderCreator(sbg::ProviderCreator<glm::mat4>& providerCreator) const {
	providerCreator.add("menu-background-scrolling", [](sbg::MainEngine& mainEngine, sbg::Entity entity, sbg::Property data) {
		return [&mainEngine, speed = float{data["speed"]}, token = std::optional<sbg::OwnershipToken>{}, position = 0.f]() mutable {
			if (!token) {
				token = mainEngine.add([&](sbg::Time delta) {
					position += speed * delta.current;
				});
			}
			
			return glm::translate(glm::mat4{1.f}, glm::vec3{position, -512, 0});
		};
	});
	
	providerCreator.add("menu-background-integral-scaling", [](sbg::Window& window, sbg::Entity entity, sbg::Property data) {
		auto baseHeight = int{data["height"]};
		
		sbg::Log::trace(SBG_LOG_INFO, "Ortho projection for menu created with base height", baseHeight);
		
		return [&window, baseHeight] {
			auto size = window.size();
			auto scaling = 1 + ((size.y) / baseHeight);
			auto height = (((size.y / scaling) % baseHeight));
			auto width = (size.x) / (scaling);
			
			return glm::ortho(
				width / -2.f, width / 2.f,
				height / 2.f, height / -2.f,
				-1.f, 100.f
			);
		};
	});
}

void MenuModule::setupModelCreator(sbg::ModelCreator& modelCreator) const {
	modelCreator.add("button-sprite", [](sbg::Entity entity, sbg::Property data) {
		auto sprite = ButtonSprite{};
		
		sprite.transform = sbg::Component<sbg::Transform>{entity};
		sprite.origin = sbg::Transform{}.scale2d(sbg::Vector2f{550, 100});
		
		return sprite;
	});
	
	modelCreator.add("button-text", [](sbg::MsdfFontManager fonts, sbg::Entity entity, sbg::Property data) {
		auto const button = sbg::Component<Button>{entity};
		auto model = sbg::TextModel{};
		
		model.align = sbg::TextHorizontalAlign::center;
		model.verticalAlign = sbg::RectVerticalAlign::middle;
		model.color = sbg::Vector4f{0, 0, 0, 1};
		model.font = sbg::FontTag{"Droid Sans"};
		model.msdf = fonts.get("Droid Sans");
		model.text = button->text;
		model.size = 48.f;
		model.transform = sbg::Component<sbg::Transform>{entity};
		
		return model;
	});
}

void MenuModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("menu", [](Menu& menu, sbg::Entity entity, sbg::Property data) {
		menu.addButton(entity);
	});
}

void MenuModule::setupEnvironmentUniformCreator(sbg::EnvironmentUniformCreator& environmentUniformCreator) const {
	using namespace sbg::literals;
	environmentUniformCreator.add("button-background", [](sbg::Property data) {
		return [
			texture_name = sbg::TextureTag{data["background"_h]}
		](sbg::EnvironmentUniformEncoder& encoder) {
			encoder
				<< sbg::uniform::resource("background"_h, texture_name);
		};
	});
}
