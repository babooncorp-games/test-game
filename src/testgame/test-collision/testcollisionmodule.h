#pragma once

namespace sbg {
	struct EntityBindingCreator;
	struct PluggerCreator;
	struct CollisionReactorCreator;
}

namespace testgame::testcollision {

struct TestCollisionModule {
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const;
	void setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const;
};

} // namespace testgame::testcollision
