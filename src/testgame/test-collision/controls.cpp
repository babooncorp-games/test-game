#include "controls.h"

#include "subgine/entity.h"
#include "subgine/physic.h"
#include "subgine/graphic.h"
#include "subgine/window.h"
#include "subgine/log.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace testgame::testcollision;

Controls::Controls(sbg::Window& window, sbg::InputTracker& tracker, sbg::Camera& camera) : _window{window}, _tracker{tracker}, _camera{camera} {}

void Controls::control(sbg::Entity entity) {
	sbg::Log::trace(SBG_LOG_INFO, "Entity set to controls, instance", this);
	_target = entity;
}

void Controls::handle(const sbg::MouseMoveEvent& event) {
	if (_target && _tracker.isMouseDown(sbg::ClickEvent::Button::Left)) {
		auto const position = event.position;
		auto const halfWindowSize = _window.size() / 2.;
		auto const projection = _camera.projection();
		auto const view = _camera.view();
		
		auto const plainCoordinate = sbg::Vector2d{1, -1} * ((position / halfWindowSize) - sbg::Vector2d{1, 1});
		auto const worldPosition = glm::inverse(projection * view) * glm::vec4{plainCoordinate.x, plainCoordinate.y, 0, 1};
		auto const physicPoint = sbg::Component<sbg::PhysicPoint2D>{_target};
		physicPoint->setPosition(sbg::Vector2d{worldPosition.x, worldPosition.y});
	}
}
