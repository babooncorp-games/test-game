#pragma once

#include "../testcollisionmodule.h"

#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"

#include "subgine/common/kangaru.h"

namespace testgame::testcollision {

struct TestCollisionModuleService : kgr::single_service<TestCollisionModule>,
	sbg::autocall<
		&TestCollisionModule::setupCollisionReactorCreator,
		&TestCollisionModule::setupEntityBindingCreator,
		&TestCollisionModule::setupPluggerCreator
	> {};

auto service_map(TestCollisionModule const&) -> TestCollisionModuleService;

} // namespace testgame::testcollision
