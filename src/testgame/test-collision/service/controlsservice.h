#pragma once

#include "subgine/common/kangaru.h"

#include "../controls.h"

#include "subgine/window/service/windowservice.h"
#include "subgine/window/service/inputtrackerservice.h"
#include "subgine/graphic/service/cameraservice.h"

namespace testgame::testcollision {

struct ControlsService : kgr::single_service<Controls, kgr::autowire> {};

auto service_map(Controls const&) -> ControlsService;

} // namespace testgame::testcollision
