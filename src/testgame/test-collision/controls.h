#pragma once

#include "subgine/window/inputtracker.h"
#include "subgine/graphic/camera.h"
#include "subgine/entity/entity.h"
#include "subgine/window/window.h"
#include "subgine/window/event/mousemoveevent.h"

namespace testgame::testcollision {

struct Controls {
	Controls(sbg::Window& window, sbg::InputTracker& tracker, sbg::Camera& camera);
	
	void control(sbg::Entity entity);
	void handle(const sbg::MouseMoveEvent& event);
	
private:
	sbg::Entity _target;
	sbg::Window& _window;
	sbg::InputTracker& _tracker;
	sbg::Camera& _camera;
};

} // namespace testgame::testcollision
