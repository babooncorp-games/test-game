#include "testcollisionmodule.h"

#include "service/controlsservice.h"

#include "subgine/collision.h"
#include "subgine/entity.h"
#include "subgine/event.h"
#include "subgine/scene.h"
#include "subgine/aseprite.h"

#include "subgine/system/service.h"
#include "subgine/entity/service.h"
#include "subgine/collision/service.h"

using namespace testgame::testcollision;

void TestCollisionModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& collisionReactorCreator) const {
	collisionReactorCreator.add("color-change", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity) {
		auto const animation = sbg::Component<sbg::AsepriteAnimation>{entity};
		
		return sbg::EventReactor{
			scheduler, entity,
			[animation]{
				animation->select("collided");
			},
			[animation]{
				animation->select("idle");
			}
		};
	});
}

void TestCollisionModule::setupEntityBindingCreator(sbg::EntityBindingCreator& entityBindingCreator) const {
	entityBindingCreator.add("mouse-controls", [](Controls& controls, sbg::Entity entity) {
		controls.control(entity);
	});
}

void TestCollisionModule::setupPluggerCreator(sbg::PluggerCreator& pluggerCreator) const {
	pluggerCreator.add("mouse-controls", [](sbg::EventDispatcher& events, Controls& controls) {
		return sbg::Plugger{
			[&events, &controls]{ events.subscribe(controls); },
			[&events, &controls]{ events.unsubscribe(controls); }
		};
	});
}
