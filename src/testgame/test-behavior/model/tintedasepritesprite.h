#pragma once

#include "subgine/aseprite/component/asepriteanimation.h"
#include "subgine/graphic/model/simplesprite.h"
#include "subgine/entity/component.h"

namespace testgame::testbehavior {

struct TintedAsepriteSprite : sbg::SimpleSprite {
	sbg::Component<sbg::AsepriteAnimation> animation;
	sbg::Vector4f tint;
	
	static void serialize(sbg::ModelUniformEncoder& uniforms, TintedAsepriteSprite const& sprite);
};

} // namespace testgame::testbehavior
