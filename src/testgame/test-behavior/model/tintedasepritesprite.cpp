#include "tintedasepritesprite.h"

#include "subgine/graphic/uniform.h"
#include "subgine/graphic/utility/transformmodelmatrix.h"

using namespace testgame::testbehavior;
using namespace sbg::literals;

void TintedAsepriteSprite::serialize(sbg::ModelUniformEncoder& uniforms, TintedAsepriteSprite const& sprite) {
	uniforms
		<< sbg::uniform::resource("spritesheet"_h, sprite.texture)
		<< sbg::uniform::updating("frame"_h, [](TintedAsepriteSprite const& self) noexcept {
			return self.animation->frame();
		})
		<< sbg::uniform::updating("model"_h, sbg::TransformModelMatrix<sbg::SimpleSprite>{})
		<< sbg::uniform::value("tint"_h, sprite.tint)
		<< sbg::uniform::value("visible"_h, sprite.visible);
}
