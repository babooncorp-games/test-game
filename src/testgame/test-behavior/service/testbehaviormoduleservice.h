#pragma once

#include "../testbehaviormodule.h"

#include "subgine/behavior/service/behaviorcreatorservice.h"
#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace testgame::testbehavior {

struct TestBehaviorModuleService : kgr::single_service<TestBehaviorModule>,
	sbg::autocall<
		&TestBehaviorModule::setupComponentCreator,
		&TestBehaviorModule::setupBehaviorCreator,
		&TestBehaviorModule::setupModelCreator
	> {};

auto service_map(TestBehaviorModule const&) -> TestBehaviorModuleService;

} // namespace testgame
