#pragma once

#include "runnerstate.h"

#include "subgine/entity/entity.h"
#include "subgine/state/utility/from.h"

#include <optional>
#include <variant>

namespace testgame::testbehavior {

struct RunnerTransition : sbg::From<Idle, Run, Jump> {
	inline explicit RunnerTransition(sbg::Vector2d movement, sbg::Entity entity) : _movement{movement}, _entity{entity} {}
	std::optional<std::variant<Run, Jump>> transit(Idle idle);
	std::optional<std::variant<Idle, Jump>> transit(Run run);
	std::optional<std::variant<Idle, Run>> transit(Jump jump);
	
private:
	sbg::Vector2d _movement;
	sbg::Entity _entity;
};

} // namespace testgame::testbehavior
