#include "runnertransition.h"

#include "subgine/aseprite.h"
#include "subgine/graphic.h"
#include "subgine/state.h"

using namespace testgame::testbehavior;

std::optional<std::variant<Run, Jump>> RunnerTransition::transit(Idle idle) {
	auto& asepriteAnimation = _entity.component<sbg::AsepriteAnimation>();
	if (_movement.y == 0) {
		if (_movement.x > 0) { // Right
			asepriteAnimation.select("move_right");
		} else if (_movement.x < 0) { // Left
			asepriteAnimation.select("move_left");
		}
		return Run{_movement.unit()};
	} else {
		if (_movement.x > 0) { // Right
			asepriteAnimation.select("jump_right");
		} else if (_movement.x < 0) { // Left
			asepriteAnimation.select("jump_left");
		}
		return Jump{_movement.unit()};
	}
}

std::optional<std::variant<Idle, Jump>> RunnerTransition::transit(Run run) {
	auto& asepriteAnimation = _entity.component<sbg::AsepriteAnimation>();
	if (_movement.y == 0) {
		if (_movement.x == 0) {
			if (run.direction.x > 0) { // Right
				asepriteAnimation.select("idle_right");
			} else if (run.direction.x < 0) { // Left
				asepriteAnimation.select("idle_left");
			}
			return Idle{run.direction.unit()};
		}
	} else {
		if (run.direction.x > 0) { // Right
			asepriteAnimation.select("jump_right");
		} else if (run.direction.x < 0) { // Left
			asepriteAnimation.select("jump_left");
		}
		return Jump{run.direction.unit()};
	}
	return {};
}

std::optional<std::variant<Idle, Run>> RunnerTransition::transit(Jump jump) {
	auto& asepriteAnimation = _entity.component<sbg::AsepriteAnimation>();
	if (_movement.y == 0) {
		if (_movement.x == 0) {
			if (jump.direction.x > 0) { // Right
				asepriteAnimation.select("idle_right");
			} else if (jump.direction.x < 0) { // Left
				asepriteAnimation.select("idle_left");
			}
			return Idle{jump.direction.unit()};
		} else {
			if (_movement.x > 0) { // Right
				asepriteAnimation.select("move_right");
			} else if (_movement.x < 0) { // Left
				asepriteAnimation.select("move_left");
			}
			return Run{_movement.unit()};
		}
	}
	return {};
}
