#pragma once

#include "subgine/vector/vector.h"

namespace testgame::testbehavior {

struct Idle {
	sbg::Vector2d direction;
};

struct Run {
	sbg::Vector2d direction;
};

struct Jump {
	sbg::Vector2d direction;
};

} // namespace testgame::testbehavior
