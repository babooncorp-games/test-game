#pragma once

namespace sbg {
	struct ComponentCreator;
	struct BehaviorCreator;
	struct ModelCreator;
}

namespace testgame::testbehavior {

struct TestBehaviorModule {
	void setupComponentCreator(sbg::ComponentCreator& componentCreator) const;
	void setupBehaviorCreator(sbg::BehaviorCreator& behaviorCreator) const;
	void setupModelCreator(sbg::ModelCreator& modelCreator) const;
};

} // namespace testgame
