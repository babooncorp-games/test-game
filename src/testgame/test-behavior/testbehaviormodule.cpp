#include "testbehaviormodule.h"

#include "testgame/test-behavior.h"
#include "testgame/test-behavior/service.h"

#include "subgine/behavior.h"
#include "subgine/entity.h"
#include "subgine/state.h"
#include "subgine/graphic.h"
#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/vector.h"

#include "subgine/behavior/service.h"
#include "subgine/entity/service.h"

using namespace testgame::testbehavior;

void TestBehaviorModule::setupComponentCreator(sbg::ComponentCreator& componentCreator) const {
	componentCreator.add("runner-statemachine", [](sbg::Entity entity) {
		return sbg::StateMachine{Idle{{1, 0}}};
	});
}

void TestBehaviorModule::setupBehaviorCreator(sbg::BehaviorCreator& behaviorCreator) const {
	behaviorCreator.add("run", [](sbg::Entity entity, sbg::Property data) {
		return sbg::Behavior{[entity]() {
			auto& stateMachine = entity.component<sbg::StateMachine>();

			auto result = stateMachine.transit(RunnerTransition{{1, 0}, entity});

			return result ? sbg::BehaviorStatus::Success : sbg::BehaviorStatus::Failure;
		}};
	});

	behaviorCreator.add("idle", [](sbg::Entity entity, sbg::Property data) {
		return sbg::Behavior{[entity]() {
			auto& stateMachine = entity.component<sbg::StateMachine>();

			auto result = stateMachine.transit(RunnerTransition{{0, 0}, entity});

			return result ? sbg::BehaviorStatus::Success : sbg::BehaviorStatus::Failure;
		}};
	});

	behaviorCreator.add("jump", [](sbg::Entity entity, sbg::Property data) {
		return sbg::Behavior{[entity]() {
			auto& stateMachine = entity.component<sbg::StateMachine>();

			auto result = stateMachine.transit(RunnerTransition{{0, 1}, entity});

			return result ? sbg::BehaviorStatus::Success : sbg::BehaviorStatus::Failure;
		}};
	});
}

void TestBehaviorModule::setupModelCreator(sbg::ModelCreator& modelCreator) const {
	modelCreator.add("tinted-aseprite-sprite", [](sbg::Entity entity, sbg::Property data) {
		auto sprite = TintedAsepriteSprite{};
		auto transform = sbg::Component<sbg::Transform>{entity};
		auto origin = sbg::apply_transform_from_property(data);
		
		sprite.origin = origin;
		sprite.transform = transform;
		sprite.animation = sbg::Component<sbg::AsepriteAnimation>{entity};
		sprite.texture = sbg::TextureTag{data["texture"]};
		sprite.tint = sbg::Vector4f{data["tint"]};
		
		return sprite;
	});
}
