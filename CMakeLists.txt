cmake_minimum_required(VERSION 3.21)

project(testgame CXX)

if(NOT subgine-pkg)
	include("sbg-modules/default-profile.cmake")
endif()

set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(subgine REQUIRED)

add_subdirectory(src)
